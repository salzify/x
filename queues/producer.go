package queues

import (
	"context"

	"github.com/gofrs/uuid/v5"
	"github.com/vmihailenco/taskq/v3"
)

type Producer struct {
	q taskq.Queue

	results ResultsBackend
}

func NewProducer(queue taskq.Queue, results ResultsBackend) *Producer {
	return &Producer{
		q:       queue,
		results: results,
	}
}

// SubmitTask submits a task by its name to be executed
func (p *Producer) SubmitTask(
	ctx context.Context,
	name string,
	args ...interface{},
) (*TaskResult, error) {
	msg := taskq.NewMessage(ctx, args...)
	msg.ID = uuid.Must(uuid.NewV4()).String()
	msg.TaskName = name
	err := p.q.Add(msg)
	if err != nil {
		return nil, err
	}

	tr := TaskResult{
		Id:        msg.ID,
		QueueName: p.q.Name(),
		TaskName:  msg.TaskName,
		Status:    TaskStatusPending,
	}

	err = p.results.Put(ctx, tr)
	if err != nil {
		return nil, err
	}

	return &tr, nil
}

func (p *Producer) GetTaskResult(
	ctx context.Context,
	id string,
) (*TaskResult, error) {
	return p.results.Get(ctx, id)
}

func (p *Producer) IsNotFound(err error) bool {
	return p.results.IsNotFound(err)
}
