package queues

import (
	"context"
	"time"

	"github.com/vmihailenco/taskq/v3"
	"go.uber.org/zap"
)

type ProducerConsumer interface {
	RegisterTasks(regs ...Registerer) error
	RegisterTask(name string, handlerFn, fallbackHandlerFn interface{}) error
	ConsumeAll(pollInterval time.Duration)

	Queue() taskq.Queue
	GetProducer() *Producer
	GetConsumer() *Consumer

	SubmitTask(ctx context.Context, name string, args ...interface{}) (*TaskResult, error)
	GetTaskResult(ctx context.Context, id string) (*TaskResult, error)
	IsNotFound(err error) bool
}

type Registerer interface {
	RegisterAsyncTasks(consumer ProducerConsumer) error
}

type producerConsumer struct {
	log *zap.SugaredLogger
	q   taskq.Queue
	*Producer
	*Consumer
}

func NewProducerConsumer(
	queueName string,
	broker string,
	retryLimit int, minBackOff, maxBackoff time.Duration,
	resultsUrl string,
	resultsExpireAfter time.Duration,
	log *zap.SugaredLogger,
) (*producerConsumer, error) {
	return NewProducerConsumerWithQueue(
		queueName,
		func(opts taskq.QueueOptions) (taskq.Queue, error) {
			return NewQueue(broker, opts)
		},
		retryLimit, minBackOff, maxBackoff,
		func() (ResultsBackend, error) {
			return NewResultBackend(resultsUrl, resultsExpireAfter)
		},
		func() ([]taskq.ConsumerHook, error) {
			return nil, nil
		},
		log,
	)
}

func NewProducerConsumerWithQueue(
	queueName string,
	getQueue func(options taskq.QueueOptions) (taskq.Queue, error),
	retryLimit int, minBackOff, maxBackoff time.Duration,
	getResults func() (ResultsBackend, error),
	getHooks func() ([]taskq.ConsumerHook, error),
	log *zap.SugaredLogger,
	options ...ApplyOptions,
) (*producerConsumer, error) {
	tasksHandler := &taskq.TaskMap{}
	opts := taskq.QueueOptions{
		Name:    queueName,
		Handler: tasksHandler,
	}
	for _, fn := range options {
		opts = fn(opts)
	}
	queue, err := getQueue(opts)
	if err != nil {
		return nil, err
	}
	results, err := getResults()
	if err != nil {
		return nil, err
	}
	hooks, err := getHooks()
	if err != nil {
		return nil, err
	}
	producer := NewProducer(queue, results)
	consumer := NewConsumer(
		queue,
		tasksHandler,
		hooks,
		results,
		retryLimit,
		minBackOff,
		maxBackoff,
		log,
	)

	return &producerConsumer{
		log:      log,
		q:        queue,
		Producer: producer,
		Consumer: consumer,
	}, nil
}

func (pc *producerConsumer) RegisterTasks(regs ...Registerer) error {
	for _, r := range regs {
		err := r.RegisterAsyncTasks(pc)
		if err != nil {
			return err
		}
	}

	return nil
}

func (pc *producerConsumer) Queue() taskq.Queue {
	return pc.q
}

func (pc *producerConsumer) GetProducer() *Producer {
	return pc.Producer
}

func (pc *producerConsumer) GetConsumer() *Consumer {
	return pc.Consumer
}

type ApplyOptions func(options taskq.QueueOptions) taskq.QueueOptions
