package queues

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"time"

	"github.com/vmihailenco/taskq/v3"
	"go.uber.org/zap"
)

const (
	resultContextKey = "__ASYNC_RESULT_KEY__"
)

type Consumer struct {
	log     *zap.SugaredLogger
	q       taskq.Queue
	tasks   *taskq.TaskMap
	results ResultsBackend

	retryLimit int
	minBackOff time.Duration
	maxBackoff time.Duration
}

func NewConsumer(
	queue taskq.Queue,
	tasks *taskq.TaskMap,
	hooks []taskq.ConsumerHook,
	resultsBackend ResultsBackend,
	retryLimit int, minBackOff, maxBackoff time.Duration,
	log *zap.SugaredLogger,
) *Consumer {
	c := &Consumer{
		log:        log,
		q:          queue,
		tasks:      tasks,
		results:    resultsBackend,
		retryLimit: retryLimit,
		minBackOff: minBackOff,
		maxBackoff: maxBackoff,
	}
	taskqConsumer := c.q.Consumer()
	taskqConsumer.AddHook(newTaskLogHook(log, c.q.Name(), true))
	for _, hook := range hooks {
		taskqConsumer.AddHook(hook)
	}
	if resultsBackend != nil {
		taskqConsumer.AddHook(c)
	}
	taskqConsumer.AddHook(newTaskLogHook(log, c.q.Name(), false))
	return c
}

// Consume starts consumer workers in background and returns
func (c *Consumer) Consume(ctx context.Context) error {
	return Consume(ctx, c.q)
}

// ConsumeUntil starts consumer workers and waits until stop channel is closed
func (c *Consumer) ConsumeUntil(ctx context.Context, stop chan struct{}) error {
	return ConsumeUntil(ctx, stop, c.q)
}

// ConsumeAll waits until all messages in the queue are processed.
func (c *Consumer) ConsumeAll(pollInterval time.Duration) {
	ConsumeAll(c.q, pollInterval)
}

// ConsumeForever starts consumer workers and returns only if os.Interrupt, os.Kill signals are sent to the process.
func (c *Consumer) ConsumeForever() error {
	return ConsumeForever(c.q)
}

// RegisterTask registers a new task to Backend
func (c *Consumer) RegisterTask(
	name string,
	handlerFn, fallbackHandlerFn interface{},
) error {
	handler, err := NewFuncHandler(handlerFn, resultContextKey)
	if err != nil {
		return err
	}

	var fallbackHandler taskq.Handler
	if fallbackHandlerFn != nil {
		var err error
		fallbackHandler, err = NewFuncHandler(fallbackHandler, resultContextKey)
		if err != nil {
			return err
		}
	}

	opts := &taskq.TaskOptions{
		Name:            name,
		Handler:         handler,
		FallbackHandler: fallbackHandler,
		DeferFunc: func() {
			if err := recover(); err != nil {
				c.log.With(zap.Stack("stack")).Errorf("panic recovered: %s", err)
			}
		},
		RetryLimit: c.retryLimit,
		MinBackoff: c.minBackOff,
		MaxBackoff: c.maxBackoff,
	}

	_, err = c.tasks.Register(opts)
	if err != nil {
		return err
	}

	return nil
}

func (c *Consumer) BeforeProcessMessage(event *taskq.ProcessMessageEvent) error {
	return c.results.Put(
		event.Message.Ctx, TaskResult{
			Id:        event.Message.ID,
			QueueName: c.q.Name(),
			TaskName:  event.Message.TaskName,
			Status:    TaskStatusStarted,
			StartedAt: &event.StartTime,
		},
	)
}

func (c *Consumer) AfterProcessMessage(event *taskq.ProcessMessageEvent) error {
	now := time.Now().UTC()
	msg := event.Message
	tResult := TaskResult{
		Id:         msg.ID,
		QueueName:  c.q.Name(),
		TaskName:   msg.TaskName,
		Status:     TaskStatusStarted,
		StartedAt:  &event.StartTime,
		FinishedAt: &now,
	}
	if msg.Err != nil {
		tResult.Err = ToTaskErr(msg.Err)
		tResult.Status = TaskStatusFailed
	} else {
		result := event.Message.Ctx.Value(resultContextKey)
		if result != nil {
			resultJ, err := json.Marshal(result)
			if err != nil {
				return fmt.Errorf("unable to encode task result: %w", err)
			}
			tResult.Result = resultJ
		}

		tResult.Status = TaskStatusSucceeded
	}

	return c.results.Put(event.Message.Ctx, tResult)
}

// Consume starts consumer workers in background and returns
func Consume(ctx context.Context, q taskq.Queue) error {
	return q.Consumer().Start(ctx)
}

// ConsumeUntil starts consumer workers and waits until stop channel is closed
func ConsumeUntil(ctx context.Context, stop chan struct{}, q taskq.Queue) error {
	err := Consume(ctx, q)
	if err != nil {
		return err
	}
	for {
		select {
		case <-stop:
			return q.Close()
		}
	}

}

// ConsumeForever starts consumer workers and returns only if os.Interrupt, os.Kill signals are sent to the process.
func ConsumeForever(q taskq.Queue) error {
	stop := make(chan struct{})

	hadErr := make(chan struct{})
	defer close(hadErr)

	// worker listening for process exist or errors to send stop signal
	go func() {
		defer close(stop)

		sig := make(chan os.Signal, 1)
		defer func() {
			signal.Stop(sig)
			close(sig)
		}()

		signal.Notify(sig, os.Interrupt, os.Kill)

		for {
			select {
			case <-sig:
				return // return to close the stop channel
			case <-hadErr:
				return // the ConsumeUntil returned an error so ConsumeForever returned and hadErr is closed we shall stop this goroutine as well
			}
		}
	}()

	return ConsumeUntil(context.Background(), stop, q)
}

// ConsumeAll waits until all messages in the queue are processed.
// NOTE: it doesn't start the consumer.
func ConsumeAll(q taskq.Queue, pollInterval time.Duration) {
	consumer := q.Consumer()
	var prev *taskq.ConsumerStats
	var noWork int
	for {
		st := consumer.Stats()
		if prev != nil &&
			st.Buffered == 0 &&
			st.InFlight == 0 &&
			st.Processed == prev.Processed {
			noWork++
			if noWork == 2 {
				return
			}
		} else {
			noWork = 0
		}
		prev = st
		time.Sleep(pollInterval)
	}
}

type taskLogHook struct {
	log       *zap.SugaredLogger
	queueName string
	before    bool
}

func newTaskLogHook(log *zap.SugaredLogger, queueName string, before bool) *taskLogHook {
	return &taskLogHook{log: log, queueName: queueName, before: before}
}

func (c *taskLogHook) BeforeProcessMessage(event *taskq.ProcessMessageEvent) error {
	if !c.before {
		return nil
	}
	c.log.Infof("starting message; queueName: %s; taskName: %s; messageName: %s; messageID: %s",
		c.queueName, event.Message.TaskName, event.Message.Name, event.Message.ID,
	)
	return nil
}

func (c *taskLogHook) AfterProcessMessage(event *taskq.ProcessMessageEvent) error {
	if c.before {
		return nil
	}
	now := time.Now().UTC()

	c.log.Infof("completed message; queueName: %s; taskName: %s; messageName: %s; messageID: %s; took: %s",
		c.queueName, event.Message.TaskName, event.Message.Name, event.Message.ID,
		now.Sub(event.StartTime),
	)

	return nil
}
