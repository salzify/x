package queues

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"reflect"

	"github.com/vmihailenco/msgpack/v5"
	"github.com/vmihailenco/taskq/v3"
)


var contextType = reflect.TypeOf((*context.Context)(nil)).Elem()
var messageType = reflect.TypeOf((*taskq.Message)(nil))
var errorType = reflect.TypeOf((*error)(nil)).Elem()

type reflectFunc struct {
	fv reflect.Value // Kind() == reflect.Func
	ft reflect.Type

	acceptsContext bool
	returnsError   bool

	returnsResult bool
	resultKey     string
}

var _ taskq.Handler = (*reflectFunc)(nil)

func NewFuncHandler(fn interface{}, resultKey string) (taskq.Handler, error) {
	fv := reflect.ValueOf(fn)

	if fv.IsNil() {
		return nil, errors.New("taskq: handler func is nil")
	}

	h := &reflectFunc{
		fv: fv,
	}
	h.ft = h.fv.Type()
	if h.ft.Kind() != reflect.Func {
		return nil, fmt.Errorf("taskq: got %s, wanted %s", h.ft.Kind(), reflect.Func)
	}

	h.returnsError = returnsError(h.ft)
	h.returnsResult = returnsResult(h.ft)
	h.resultKey = resultKey
	if acceptsMessage(h.ft) {
		return nil, fmt.Errorf("task handler should not accept low-level *taskq.Message")
	}

	h.acceptsContext = acceptsContext(h.ft)
	return h, nil
}

func (h *reflectFunc) HandleMessage(msg *taskq.Message) error {
	in, err := h.fnArgs(msg)
	if err != nil {
		return err
	}

	out := h.fv.Call(in)
	if h.returnsError {
		errv := out[h.ft.NumOut()-1]
		if !errv.IsNil() {
			return errv.Interface().(error)
		}
	}
	if h.returnsResult {
		result := out[0].Interface()
		msg.Ctx = context.WithValue(msg.Ctx, h.resultKey, result)
	}

	return nil
}

func (h *reflectFunc) fnArgs(msg *taskq.Message) ([]reflect.Value, error) {
	in := make([]reflect.Value, h.ft.NumIn())
	inSaved := in

	var inStart int
	if h.acceptsContext {
		inStart = 1
		in[0] = reflect.ValueOf(msg.Ctx)
		in = in[1:]
	}

	if len(msg.Args) == len(in) {
		var hasWrongType bool
		for i, arg := range msg.Args {
			v := reflect.ValueOf(arg)
			inType := h.ft.In(inStart + i)

			if inType.Kind() == reflect.Interface {
				if !v.Type().Implements(inType) {
					hasWrongType = true
					break
				}
			} else if v.Type() != inType {
				hasWrongType = true
				break
			}

			in[i] = v
		}
		if !hasWrongType {
			return inSaved, nil
		}
	}

	b, err := msg.MarshalArgs()
	if err != nil {
		return nil, err
	}

	dec := msgpack.NewDecoder(bytes.NewBuffer(b))
	n, err := dec.DecodeArrayLen()
	if err != nil {
		return nil, err
	}

	if n == -1 {
		n = 0
	}
	if n != len(in) {
		return nil, fmt.Errorf("taskq: got %d args, wanted %d", n, len(in))
	}

	for i := 0; i < len(in); i++ {
		arg := reflect.New(h.ft.In(inStart + i)).Elem()
		err = dec.DecodeValue(arg)
		if err != nil {
			err = fmt.Errorf(
				"taskq: decoding arg=%d failed (data=%.100x): %s", i, b, err)
			return nil, err
		}
		in[i] = arg
	}

	return inSaved, nil
}

func acceptsMessage(typ reflect.Type) bool {
	return typ.NumIn() == 1 && typ.In(0) == messageType
}

func acceptsContext(typ reflect.Type) bool {
	return typ.NumIn() > 0 && typ.In(0).Implements(contextType)
}

func returnsError(typ reflect.Type) bool {
	n := typ.NumOut()
	return n > 0 && typ.Out(n-1) == errorType
}
func returnsResult(typ reflect.Type) bool {
	n := typ.NumOut()
	return n > 0 && typ.Out(0) != errorType
}
