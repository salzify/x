package queues

import (
	"bytes"
	"context"
	"fmt"
	"math/rand"
	"reflect"
	"time"

	"github.com/vmihailenco/msgpack/v5"
	"github.com/vmihailenco/taskq/v3"
	"go.uber.org/zap"
)

func NewCtxArgsProducerConsumer(
	queueName, broker string,
	retryLimit int,
	minBackoff, maxBackoff time.Duration,
	log *zap.SugaredLogger,
	getResults func() (ResultsBackend, error),
	packer msgPacker, unpacker msgUnpacker,
) (*ctxArgsWrapper, error) {
	pc, err := NewProducerConsumerWithQueue(
		queueName,
		func(opts taskq.QueueOptions) (taskq.Queue, error) {
			return NewQueue(broker, opts)
		},
		retryLimit,
		minBackoff,
		maxBackoff,
		getResults,
		func() ([]taskq.ConsumerHook, error) {
			return []taskq.ConsumerHook{newCtxArgsHook(unpacker)}, nil
		},
		log,
		func(opts taskq.QueueOptions) taskq.QueueOptions {
			opts.MinNumWorker = 4
			return opts
		},
	)
	if err != nil {
		return nil, err
	}
	return newCtxArgsWrapper(pc, packer), nil
}

type PackedMessage interface{}

type msgPacker interface {
	PackMsg(ctx context.Context, name string, args []interface{}) (PackedMessage, error)
}

type msgUnpacker interface {
	UnpackMsg(ctx context.Context, msg PackedMessage) (context.Context, []interface{}, error)
	NewMsg() interface{}
}

var (
	ctxArgsStashKey = struct{ i int }{rand.Int()}
)

type ctxArgsStash struct {
	ctx     context.Context
	argsBin []byte
}

type ctxArgsWrapper struct {
	ProducerConsumer
	msgPacker msgPacker
}

func newCtxArgsWrapper(producerConsumer ProducerConsumer, msgPacker msgPacker) *ctxArgsWrapper {
	return &ctxArgsWrapper{ProducerConsumer: producerConsumer, msgPacker: msgPacker}
}

func (p *ctxArgsWrapper) SubmitTask(
	ctx context.Context,
	name string,
	args ...interface{},
) (*TaskResult, error) {
	msg, err := p.msgPacker.PackMsg(ctx, name, args)
	if err != nil {
		return nil, err
	}

	return p.ProducerConsumer.SubmitTask(ctx, name, msg)
}

type CtxArgsHook struct {
	msgUnpacker msgUnpacker
}

func newCtxArgsHook(msgUnpacker msgUnpacker) *CtxArgsHook {
	return &CtxArgsHook{msgUnpacker: msgUnpacker}
}

func (h *CtxArgsHook) BeforeProcessMessage(event *taskq.ProcessMessageEvent) error {
	msg := event.Message
	if event.Stash == nil {
		event.Stash = map[interface{}]interface{}{}
	}

	b, err := msg.MarshalArgs()
	if err != nil {
		return err
	}

	dec := msgpack.NewDecoder(bytes.NewBuffer(b))
	n, err := dec.DecodeArrayLen()
	if err != nil {
		return err
	}

	if n == -1 {
		n = 0
	}
	if n != 1 {
		return fmt.Errorf("invalid rawArgs length, expected 1 got %v", n)
	}

	packedMsg := h.msgUnpacker.NewMsg()
	argsV := reflect.ValueOf(packedMsg)
	err = dec.DecodeValue(argsV)
	if err != nil {
		return fmt.Errorf("taskq: decoding args failed (data=%.100x): %s", b, err)
	}

	ctx := msg.Ctx

	if msg.Ctx == nil {
		ctx = context.TODO()
	}

	ctx, args, err := h.msgUnpacker.UnpackMsg(ctx, packedMsg)
	if err != nil {
		return err
	}

	msg.Ctx = ctx

	event.Stash[ctxArgsStashKey] = ctxArgsStash{
		ctx:     msg.Ctx,
		argsBin: msg.ArgsBin,
	}

	bb, err := msgpack.Marshal(args)
	if err != nil {
		return err
	}
	msg.ArgsBin = bb

	return nil
}

func (h *CtxArgsHook) AfterProcessMessage(event *taskq.ProcessMessageEvent) error {
	original := event.Stash[ctxArgsStashKey].(ctxArgsStash)

	msg := event.Message

	// FIXME: not CtxArgsHook responsibility
	if result := msg.Ctx.Value(resultContextKey); result != nil {
		original.ctx = context.WithValue(original.ctx, resultContextKey, result)
	}

	msg.Ctx = original.ctx
	msg.ArgsBin = original.argsBin
	return nil
}
