package queues

import (
	"fmt"
	"net/url"
	"strconv"
	"strings"

	"github.com/go-redis/redis/v8"
)

func NewRedisClient(url string) (*redis.Client, error) {
	if strings.HasPrefix(url, "redis-sentinel://") {
		rOpts, err := ParseRedisSentinelURL(url)
		if err != nil {
			return nil, err
		}
		return redis.NewFailoverClient(rOpts), nil
	} else {
		rOpts, err := redis.ParseURL(url)
		if err != nil {
			return nil, err
		}

		return redis.NewClient(rOpts), nil
	}
}

func ParseRedisSentinelURL(rawUrl string) (*redis.FailoverOptions, error) {
	if !strings.HasPrefix(rawUrl, "redis-sentinel://") {
		return nil, fmt.Errorf("not redis-sentinel url")
	}
	rawUrl = strings.TrimPrefix(rawUrl, "redis-sentinel://")
	opts := redis.FailoverOptions{}
	if strings.Contains(rawUrl, "@") {
		parts := strings.Split(rawUrl, "@")
		if len(parts) != 2 {
			return nil, fmt.Errorf("invalud auth part")
		}
		rawUrl = parts[1]
		authParts := strings.Split(parts[0], ":")
		if len(authParts) > 2 {
			return nil, fmt.Errorf("invalud auth parts")
		}
		opts.Username = authParts[0]
		if len(authParts) > 1 {
			opts.Password = authParts[1]
		}
	}

	paramsIdx := strings.Index(rawUrl, "/")
	if paramsIdx == -1 {
		paramsIdx = strings.Index(rawUrl, "?")
	}

	if paramsIdx != -1 {
		pathPart := rawUrl[paramsIdx+1:] // skip the /
		rawUrl = rawUrl[:paramsIdx]
		u, err := url.Parse(pathPart)
		if err != nil {
			return nil, err
		}
		if u.Path != "" {
			db, err := strconv.Atoi(u.Path)
			if err != nil {
				return nil, err
			}
			opts.DB = db
		}
		q := u.Query()
		for k := range q {
			switch k {
			case "master_name":
				opts.MasterName = q.Get(k)
			case "sentinel_password":
				opts.SentinelPassword = q.Get(k)
			}
			// TODO: remaining cases
		}
	}

	opts.SentinelAddrs = strings.Split(rawUrl, ",")
	return &opts, nil
}

func IsRedisURL(url string) bool {
	return strings.HasPrefix(url, "redis://") || strings.HasPrefix(url, "redis-sentinel://")
}
