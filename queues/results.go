package queues

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/go-redis/redis/v8"
	lru "github.com/hashicorp/golang-lru"
	"github.com/hashicorp/golang-lru/simplelru"
	"github.com/pkg/errors"
	"github.com/vmihailenco/msgpack/v5"
)

var (
	ErrNotFound = errors.New("task result not found")
)

type TaskStatus string

func (s TaskStatus) String() string {
	return string(s)
}

const (
	TaskStatusPending   = "pending"
	TaskStatusStarted   = "started"
	TaskStatusSucceeded = "succeeded"
	TaskStatusFailed    = "failed"
)

type TaskResult struct {
	Id         string          `json:"id" msgpack:"id"`
	QueueName  string          `json:"queue_name" msgpack:"queue_name"`
	TaskName   string          `json:"task_name" msgpack:"task_name"`
	Status     TaskStatus      `json:"status" msgpack:"status"`
	StartedAt  *time.Time      `json:"started_at" msgpack:"started_at"`
	FinishedAt *time.Time      `json:"finished_at" msgpack:"finished_at"`
	Result     json.RawMessage `json:"result" msgpack:"result"`
	Err        *TaskError      `json:"err" msgpack:"err"`
}

type TaskError struct {
	ID      string `json:"id"`
	Message string `json:"message"`
	Err     string `json:"err"`
}

func NewTaskError(id, message string, err error) *TaskError {
	return &TaskError{
		ID:      id,
		Message: message,
		Err:     err.Error(),
	}
}

func (e *TaskError) Error() string {
	return e.Err
}

func ToTaskErr(err error) *TaskError {
	if e, ok := err.(*TaskError); ok {
		return e
	}
	return NewTaskError("internal-error", "Unexpected error occurred", err)
}

type ResultsBackend interface {
	IsNotFound(err error) bool
	Get(ctx context.Context, id string) (*TaskResult, error)
	Put(ctx context.Context, result TaskResult) error
}

func NewResultBackend(url string, expireAfter time.Duration) (ResultsBackend, error) {
	var backend ResultsBackend
	var err error

	if url == "memory" {
		backend = NewMemoryBackend()
	} else if IsRedisURL(url) {
		backend, err = NewRedisBackend(url, expireAfter, "")
	} else {
		err = fmt.Errorf(
			"not supported result backend: %s. Only 'memory' and a redis uri are supported ",
			url,
		)
	}

	return backend, err
}

func NewRedisBackend(url string, expireAfter time.Duration, prefix string) (ResultsBackend, error) {
	client, err := NewRedisClient(url)
	if err != nil {
		return nil, err
	}

	return NewRedisBackendWithClient(client, expireAfter, prefix), nil
}

func NewMemoryBackend() ResultsBackend {
	return newMemBackend()
}

type memBackend struct {
	c simplelru.LRUCache
}

var _ ResultsBackend = (*memBackend)(nil)

func newMemBackend() *memBackend {
	c, err := lru.New(1048576) // 1MB
	if err != nil {
		panic(err) // should only happens if the lru size is less than 1
	}
	return &memBackend{
		c: c,
	}
}

func (s *memBackend) IsNotFound(err error) bool {
	return errors.Is(err, ErrNotFound)
}

func (s *memBackend) Get(ctx context.Context, id string) (*TaskResult, error) {
	v, ok := s.c.Get(id)
	if !ok {
		return nil, ErrNotFound
	}

	tr := v.(TaskResult)

	return &tr, nil
}

func (s *memBackend) Put(ctx context.Context, result TaskResult) error {
	s.c.Add(result.Id, result)
	return nil
}

type redisBackend struct {
	redis       *redis.Client
	expireAfter time.Duration
	prefix      string
}

var _ ResultsBackend = (*redisBackend)(nil)

func NewRedisBackendWithClient(redis *redis.Client, expireAfter time.Duration, prefix string) *redisBackend {
	const defaultPrefix = "results"
	if prefix == "" {
		prefix = defaultPrefix
	}
	return &redisBackend{
		redis:       redis,
		expireAfter: expireAfter,
		prefix:      prefix,
	}
}

func (s *redisBackend) key(id string) string {
	return fmt.Sprintf("%s:%s", s.prefix, id)
}

func (s *redisBackend) IsNotFound(err error) bool {
	return errors.Is(err, ErrNotFound)
}

func (s *redisBackend) Get(ctx context.Context, id string) (*TaskResult, error) {
	b, err := s.redis.Get(ctx, s.key(id)).Bytes()
	if err != nil {
		return nil, err
	}
	if len(b) == 0 {
		return nil, ErrNotFound
	}
	tr := TaskResult{}
	err = msgpack.Unmarshal(b, &tr)
	if err != nil {
		return nil, err
	}

	return &tr, nil
}

func (s *redisBackend) Put(ctx context.Context, result TaskResult) error {
	b, err := msgpack.Marshal(result)
	if err != nil {
		return err
	}
	return s.redis.Set(ctx, s.key(result.Id), b, s.expireAfter).Err()
}
