package queues

import (
	"context"
	"fmt"
	"sync"

	"github.com/hashicorp/golang-lru/simplelru"
	"github.com/vmihailenco/taskq/v3"
	"github.com/vmihailenco/taskq/v3/memqueue"
	"github.com/vmihailenco/taskq/v3/redisq"
)

func NewQueue(broker string, opts taskq.QueueOptions) (taskq.Queue, error) {
	var queue taskq.Queue
	var err error

	if broker == "memory" {
		queue, err = NewMemoryQueue(opts)
	} else if IsRedisURL(broker) {
		queue, err = NewRedisQueue(broker, opts)
	} else {
		err = fmt.Errorf(
			"not supported broker: %s. Only 'memory' and a redis uri are supported ",
			broker,
		)
	}

	return queue, err
}

func NewRedisQueue(url string, opts taskq.QueueOptions) (taskq.Queue, error) {
	client, err := NewRedisClient(url)
	if err != nil {
		return nil, err
	}
	opts.Redis = client

	return redisq.NewQueue(&opts), nil
}

func NewMemoryQueue(opts taskq.QueueOptions) (taskq.Queue, error) {
	storage, err := NewMemStorage()
	if err != nil {
		return nil, err
	}
	opts.Storage = storage

	return memqueue.NewQueue(&opts), nil
}

type memStorage struct {
	mu    sync.Mutex
	cache *simplelru.LRU
}

var _ taskq.Storage = (*memStorage)(nil)

func NewMemStorage() (*memStorage, error) {
	c, err := simplelru.NewLRU(128000, nil)
	if err != nil {
		return nil, err
	}
	return &memStorage{
		cache: c,
	}, nil
}

func (s *memStorage) Exists(ctx context.Context, key string) bool {
	s.mu.Lock()
	defer s.mu.Unlock()

	_, ok := s.cache.Get(key)
	if ok {
		return true
	}

	s.cache.Add(key, nil)
	return false

}
