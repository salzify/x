package tenancy

import (
	"errors"
)

var (
	ErrTenantNotFound  = errors.New("tenant not fround")
	ErrNotSystemTenant = errors.New("not system tenant")
)
