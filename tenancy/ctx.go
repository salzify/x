package tenancy

import (
	"context"
	"fmt"
	"math/rand"
)

var (
	tenantCtxKey = struct{ i int }{i: rand.Int()}
)

func WithCurrentTenant(ctx context.Context, tenant Tenant) context.Context {
	if _, err := GetCurrentTenant(ctx); err == nil {
		panic("can't change tenant info")
	}
	return context.WithValue(ctx, tenantCtxKey, tenant)
}

func GetCurrentTenant(ctx context.Context) (Tenant, error) {
	tenant, ok := ctx.Value(tenantCtxKey).(Tenant)
	if !ok {
		return nil, fmt.Errorf("context is missing tenant info")
	}
	return tenant, nil
}

func WithSystemTenant(ctx context.Context) context.Context {
	return WithCurrentTenant(ctx, *newSystemTenant())
}

func ValidateSystemTenant(ctx context.Context) error {
	tenant, err := GetCurrentTenant(ctx)
	if err != nil {
		return err
	}
	if !tenant.IsSystem() {
		return ErrNotSystemTenant
	}
	return nil
}
