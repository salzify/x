package tenancy

import (
	"context"
)

func IterTenants(ctx context.Context, lister Discovery, fn func(ctx context.Context, tenant Tenant) error) error {
	tenants, err := lister.ListTenants(WithSystemTenant(ctx))
	if err != nil {
		return err
	}
	for _, tenant := range tenants {
		err := fn(WithCurrentTenant(ctx, tenant), tenant)
		if err != nil {
			return err
		}
	}
	return nil
}
