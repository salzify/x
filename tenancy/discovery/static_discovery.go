package discovery

import (
	"context"
	"fmt"

	"go.e.salzify.com/x/tenancy"
)

type staticTenantDiscovery struct {
	tenantName string
	standalone bool
}

func NewStaticTenantDiscovery(tenantName string, standalone bool) *staticTenantDiscovery {
	return &staticTenantDiscovery{tenantName: tenantName, standalone: standalone}
}

func (d *staticTenantDiscovery) ListTenants(ctx context.Context) ([]tenancy.Tenant, error) {
	if err := tenancy.ValidateSystemTenant(ctx); err != nil {
		return nil, err
	}

	return []tenancy.Tenant{d.getTenant()}, nil
}

func (d *staticTenantDiscovery) GetTenant(ctx context.Context, tenantName string) (tenancy.Tenant, error) {
	if err := tenancy.ValidateSystemTenant(ctx); err != nil {
		return nil, err
	}
	if tenantName != d.tenantName {
		return nil, fmt.Errorf("%w: tenantName: %s", tenancy.ErrTenantNotFound, tenantName)
	}

	return d.getTenant(), nil
}

func (d *staticTenantDiscovery) getTenant() tenancy.Tenant {
	if d.standalone {
		return tenancy.NewStandaloneTenant(d.tenantName)
	} else {
		return tenancy.NewStaticTenant(d.tenantName)
	}
}
