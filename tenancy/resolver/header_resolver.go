package resolver

import (
	"context"
	"errors"
	"fmt"
	"net/http"

	"go.e.salzify.com/x/tenancy"
)

type headerResolver struct {
	headerName string
}

func NewHeaderResolver(headerName string) *headerResolver {
	return &headerResolver{headerName: headerName}
}

func (hr *headerResolver) ResolveTenantLookup(r *http.Request) (string, error) {
	tenantName := r.Header.Get(hr.headerName)
	if tenantName == "" {
		return "", errors.New("missing tenant header")
	}

	return tenantName, nil
}

type HeaderValuer func(context.Context, tenancy.Tenant) (string, error)

type headerApplier struct {
	headerName string
	valuer     HeaderValuer
}

func NewHeaderApplier(headerName string, valuer HeaderValuer) *headerApplier {
	return &headerApplier{headerName: headerName, valuer: valuer}
}

func (ha *headerApplier) ApplyTenantLookup(r *http.Request, tenant tenancy.Tenant) (*http.Request, error) {
	v, err := ha.valuer(r.Context(), tenant)
	if err != nil {
		return nil, err
	}
	if v == "" {
		return nil, fmt.Errorf("invalid tenant header value")
	}
	r.Header.Set(ha.headerName, v)

	return r, nil
}

func HeaderValuerTenantName(ctx context.Context, tenant tenancy.Tenant) (string, error) {
	if tenant.GetName() == "" {
		return "", fmt.Errorf("missing tenant name")
	}
	return tenant.GetName(), nil
}
