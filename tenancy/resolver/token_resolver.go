package resolver

import (
	"context"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/gofrs/uuid/v5"
	"github.com/golang-jwt/jwt/v5"

	"go.e.salzify.com/x/tenancy"
)

type tokenResolver struct {
	headerName string
	tokenKey   *rsa.PublicKey
}

func NewTokenResolver(headerName string, tokenKey *rsa.PublicKey) *tokenResolver {
	return &tokenResolver{headerName: headerName, tokenKey: tokenKey}
}

func (tr *tokenResolver) ResolveTenant(r *http.Request) (tenancy.Tenant, error) {
	headerName := tr.headerName
	values := r.Header.Values(headerName)
	if len(values) > 1 {
		return nil, fmt.Errorf("tenancy: multiple header values; header: %s; values: %v", headerName, values)
	}
	if len(values) == 0 {
		return nil, fmt.Errorf("tenancy: missing header value; header: %s", headerName)
	}

	signedToken := values[0]

	claims := &tokenClaims{}
	_, err := jwt.ParseWithClaims(
		signedToken,
		claims,
		func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
				return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
			}
			return tr.tokenKey, nil
		},
	)
	if err != nil {
		return nil, err
	}

	return &claims.CurrentTenant, nil
}

func ParseRSAPubKey(keyPath string) (*rsa.PublicKey, error) {
	pub, err := ioutil.ReadFile(keyPath)
	if err != nil {
		return nil, err
	}
	pubPem, _ := pem.Decode(pub)
	if pubPem == nil {
		return nil, fmt.Errorf("empty pem.Decode result")
	}
	if pubPem.Type != "RSA PUBLIC KEY" {
		return nil, fmt.Errorf("RSA public key is of the wrong type, PemType :%s", pubPem.Type)
	}

	parsedKey, err := x509.ParsePKIXPublicKey(pubPem.Bytes)
	if err != nil {
		return nil, err
	}

	pubKey, ok := parsedKey.(*rsa.PublicKey)
	if !ok {
		return nil, fmt.Errorf("unable to parse RSA public key")
	}

	return pubKey, nil
}

type tokenClaims struct {
	jwt.RegisteredClaims
	CurrentTenant tenancy.StandardTenant `json:"cten"`
}

func newTokenClaims(registeredClaims jwt.RegisteredClaims, currentTenant tenancy.StandardTenant) *tokenClaims {
	return &tokenClaims{RegisteredClaims: registeredClaims, CurrentTenant: currentTenant}
}

func (c tokenClaims) verifySubject() bool {
	if c.Subject == "" {
		return false
	}
	return c.Subject == c.CurrentTenant.GetID()
}

func (c tokenClaims) Validate() error {
	if c.verifySubject() == false {
		return fmt.Errorf("token subject is not valid")
	}

	return nil
}

type tokenIssuer struct {
	key *rsa.PrivateKey
	aud string
	iss string
	exp time.Duration
	nbf time.Duration
}

func NewTokenIssuer(
	key *rsa.PrivateKey,
	aud string,
	iss string,
	exp time.Duration,
	nbf time.Duration,
) *tokenIssuer {
	return &tokenIssuer{
		key: key,
		aud: aud,
		iss: iss,
		exp: exp,
		nbf: nbf,
	}
}

func (i *tokenIssuer) now() time.Time {
	return time.Now().Truncate(time.Second).UTC()
}
func (i *tokenIssuer) id() string {
	return uuid.Must(uuid.NewV4()).String()
}

func (i *tokenIssuer) registeredClaims(sub string) jwt.RegisteredClaims {
	now := i.now()
	return jwt.RegisteredClaims{
		ID:        i.id(),
		Audience:  []string{i.aud},
		ExpiresAt: jwt.NewNumericDate(now.Add(i.exp)),
		IssuedAt:  jwt.NewNumericDate(now),
		Issuer:    i.iss,
		NotBefore: jwt.NewNumericDate(now.Add(-i.nbf)),
		Subject:   sub,
	}
}

func (i *tokenIssuer) IssueToken(tenant tenancy.Tenant) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodRS256, newTokenClaims(
		i.registeredClaims(tenant.GetID()),
		*tenancy.NewStandardTenantFromTenant(tenant),
	))
	return token.SignedString(i.key)
}

type issuer interface {
	IssueToken(tenant tenancy.Tenant) (string, error)
}

type tokenHeaderGetter struct {
	issuer issuer
}

func HeaderValuerToken(iss issuer) HeaderValuer {
	return func(ctx context.Context, tenant tenancy.Tenant) (string, error) {
		return iss.IssueToken(tenant)
	}
}

func ParseRSAPrivateKey(keyPath string) (*rsa.PrivateKey, error) {
	prv, err := ioutil.ReadFile(keyPath)
	if err != nil {
		return nil, err
	}
	prvPem, _ := pem.Decode(prv)
	if prvPem == nil {
		return nil, fmt.Errorf("empty pem.Decode result")
	}
	if prvPem.Type != "RSA PRIVATE KEY" {
		return nil, fmt.Errorf("RSA private key is of the wrong type, PemType :%s", prvPem.Type)
	}

	parsedKey, err := x509.ParsePKCS1PrivateKey(prvPem.Bytes)
	if err != nil {
		return nil, err
	}

	return parsedKey, nil
}
