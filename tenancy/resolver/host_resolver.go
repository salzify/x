package resolver

import (
	"fmt"
	"net/http"
	"regexp"
)

type hostResolver struct {
	regex *regexp.Regexp
}

func NewHostResolver(regex *regexp.Regexp) *hostResolver {
	return &hostResolver{regex: regex}
}

func (hr *hostResolver) ResolveTenantLookup(r *http.Request) (string, error) {
	matches := hr.regex.FindStringSubmatch(r.Host)
	if len(matches) == 0 {
		return "", fmt.Errorf("request host is not a tenant domain; host:%s", r.Host)
	}
	tenantName := matches[1]
	return tenantName, nil
}
