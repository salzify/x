package resolver

import (
	"net/http"
)

type staticResolver struct {
	tenantName string
}

func NewStaticResolver(tenantName string) *staticResolver {
	return &staticResolver{tenantName: tenantName}
}

func (s *staticResolver) ResolveTenantLookup(r *http.Request) (string, error) {
	return s.tenantName, nil
}
