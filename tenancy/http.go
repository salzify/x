package tenancy

import (
	"net/http"
)

type HttpTenantLookupResolver interface {
	ResolveTenantLookup(r *http.Request) (string, error)
}

type HttpTenantLookupApplier interface {
	ApplyTenantLookup(r *http.Request, tenant Tenant) (*http.Request, error)
}

type HttpTenantResolver interface {
	ResolveTenant(r *http.Request) (Tenant, error)
}

type middleware struct {
	resolver   HttpTenantResolver
	errHandler func(w http.ResponseWriter, r *http.Request, err error)
}

func NewMiddleware(
	resolver HttpTenantResolver,
	errHandler func(w http.ResponseWriter, r *http.Request, err error),
) *middleware {
	if errHandler == nil {
		errHandler = defaultErrorHandler
	}
	return &middleware{
		resolver:   resolver,
		errHandler: errHandler,
	}
}

func (mw *middleware) Handler(h http.Handler) http.Handler {
	return http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			tenant, err := mw.resolver.ResolveTenant(r)
			if err != nil {
				mw.errHandler(w, r, err)
				return
			}
			h.ServeHTTP(w, r.WithContext(WithCurrentTenant(r.Context(), tenant)))
		},
	)
}

func defaultErrorHandler(w http.ResponseWriter, r *http.Request, err error) {
	w.WriteHeader(http.StatusUnauthorized)
}
