package tenancy

import (
	"context"
)

type Discovery interface {
	GetTenant(ctx context.Context, tenantLookup string) (Tenant, error)
	ListTenants(ctx context.Context) ([]Tenant, error)
}
