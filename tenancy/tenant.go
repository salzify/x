package tenancy

import (
	"encoding/json"
	"strings"

	"github.com/gofrs/uuid/v5"
)

// StandardTenant provide simple implementation for the Tenant interface.
// It can be used as a DTO object for any Tenant implementation by wrapping everything in Config field.
type StandardTenant struct {
	ID     string             `json:"id"`
	Name   string             `json:"name"`
	Active bool               `json:"active"`
	DB     DBSettings         `json:"db"`
	Config *TenantExtraConfig `json:"config"`

	Sys        bool `json:"sys"`
	Standalone bool `json:"standalone"`
}

func NewStandardTenant(
	id string,
	name string,
	active bool,
	dbSettings DBSettings,
) *StandardTenant {
	return &StandardTenant{
		ID:     id,
		Name:   name,
		Active: active,
		DB:     dbSettings,
	}
}

func (t StandardTenant) GetID() string {
	return t.ID
}

func (t StandardTenant) GetName() string {
	return t.Name
}

func (t StandardTenant) IsActive() bool {
	return t.Active
}

func (t StandardTenant) DBSettings() DBSettings {
	return t.DB
}

func (t StandardTenant) ExtraConfig() *TenantExtraConfig {
	return t.Config
}

func (t StandardTenant) IsSystem() bool {
	return t.Sys
}

func (t StandardTenant) IsStandalone() bool {
	return t.Standalone
}

func (t StandardTenant) withStandalone() *StandardTenant {
	t.Standalone = true
	return &t
}

type DBTier string

func (t DBTier) String() string {
	return string(t)
}

const (
	DBTierDedicated     DBTier = "dedicated"
	DBTierSharedSchemas DBTier = "shared:schemas"
)

func (t DBTier) IsDedicated() bool {
	return t == DBTierDedicated
}

type DBSettings struct {
	DBTier     DBTier `json:"db_tier"`
	SchemaName string `json:"schema_name"`
	// TODO: support different shared/dedicated modes config
}

func NewDBSettings(dbTier DBTier, schemaName string) *DBSettings {
	return &DBSettings{SchemaName: schemaName, DBTier: dbTier}
}

func NewStandaloneTenant(tenantName string) *StandardTenant {
	return NewStaticTenant(tenantName).withStandalone()
}

func newSystemTenant() *StandardTenant {
	return &StandardTenant{Active: true, Sys: true}
}

func NewStaticTenant(tenantName string) *StandardTenant {
	return NewStandardTenant(
		StaticTenantID(tenantName),
		tenantName,
		true,
		*NewDBSettings(DBTierDedicated, ""),
	)
}

func StaticTenantID(tenantName string) string {
	return uuid.NewV5(uuid.NamespaceDNS, NormalizeTenantName(tenantName)).String()
}

func NormalizeTenantName(name string) string {
	return strings.ToLower(strings.ReplaceAll(name, " ", ""))
}

type Tenant interface {
	GetID() string
	GetName() string
	IsActive() bool
	DBSettings() DBSettings
	ExtraConfig() *TenantExtraConfig
	IsSystem() bool
	IsStandalone() bool
}

type TenantExtraConfig struct {
	d interface{}
	j []byte
}

func (c *TenantExtraConfig) MarshalJSON() ([]byte, error) {
	if len(c.j) > 0 {
		return c.j, nil
	}
	err := c.marshalJson()
	if err != nil {
		return nil, err
	}
	return c.j, nil
}

func (c *TenantExtraConfig) marshalJson() error {
	j, err := json.Marshal(c.d)
	if err != nil {
		return err
	}
	c.j = j
	return nil
}

func (c *TenantExtraConfig) UnmarshalJSON(in []byte) error {
	c.j = in
	return nil
}

func (c *TenantExtraConfig) Get(config interface{}) error {
	if len(c.j) == 0 && c.d != nil {
		if err := c.marshalJson(); err != nil {
			return err
		}
	}
	return json.Unmarshal(c.j, config)
}

func NewTenantExtraConfig(config interface{}) *TenantExtraConfig {
	return &TenantExtraConfig{d: config}
}

func NewStandardTenantFromTenant(tenant Tenant) *StandardTenant {
	if tenant.IsSystem() {
		return newSystemTenant()
	}
	t := NewStandardTenant(
		tenant.GetID(),
		tenant.GetName(),
		tenant.IsActive(),
		tenant.DBSettings(),
	)
	if t.IsStandalone() {
		t = t.withStandalone()
	}
	t.Config = tenant.ExtraConfig()
	return t
}
