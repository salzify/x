package events

import (
	"context"

	"go.uber.org/zap"

	"go.e.salzify.com/x/queues"
)

// queuedEmitter fire events to a queue
type queuedEmitter struct {
	log      *zap.Logger
	taskName string

	pc queues.ProducerConsumer
}

func NewQueuedEmitter(log *zap.Logger, taskName string, pc queues.ProducerConsumer) *queuedEmitter {
	return &queuedEmitter{
		log:      log,
		taskName: taskName,
		pc:       pc,
	}
}

func (e *queuedEmitter) RegisterListeners(listeners ...Listener) {
	e.log.Warn("queued emitter don't support registering listeners")
}

func (e *queuedEmitter) Emit(ctx context.Context, event *Event) error {
	defer func() {
		if err := recover(); err != nil {
			e.log.Error("panic recovered", zap.Any("err", err), zap.Stack("stack"))
		}
	}()

	_, err := e.pc.SubmitTask(ctx, e.taskName, event)
	if err != nil {
		e.log.Error("unable to emit event", zap.Error(err))
	}
	return err
}

// queuedEmitterRouter listens to events fired by queuedEmitter through a queue
// and exposes default Emitter interface that can be used with normal listeners
type queuedEmitterRouter struct {
	Emitter

	log      *zap.Logger
	taskName string
}

func NewQueuedEmitterRouter(
	log *zap.Logger,
	emitter Emitter,
	taskName string,
) *queuedEmitterRouter {
	if emitter == nil {
		emitter = NewEmitter()
	}
	return &queuedEmitterRouter{
		Emitter:  emitter,
		log:      log,
		taskName: taskName,
	}
}

func (r *queuedEmitterRouter) RegisterAsyncTasks(pc queues.ProducerConsumer) error {
	return pc.RegisterTask(r.taskName, r.handler, nil)
}

func (r *queuedEmitterRouter) handler(ctx context.Context, event *Event) error {
	return r.Emit(ctx, event)
}
