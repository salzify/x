package events

import (
	"context"
	"fmt"
	"time"

	"go.uber.org/zap"
)

type topicListener struct {
	topic    EventTopic
	listener Listener
}

func NewTopicListener(eventTopic EventTopic, listener Listener) *topicListener {
	return &topicListener{topic: eventTopic, listener: listener}
}

func (l *topicListener) OnEvent(ctx context.Context, event *Event) error {
	topic := event.Topic
	if l.topic != topic {
		return nil
	}
	return l.listener.OnEvent(ctx, event)
}

type typesListener struct {
	eventType map[EventType]struct{}
	listener  Listener
}

func NewTypesListener(eventTypes []EventType, listener Listener) *typesListener {
	et := map[EventType]struct{}{}
	for _, eventType := range eventTypes {
		et[eventType] = struct{}{}
	}
	return &typesListener{eventType: et, listener: listener}
}

func NewTypeListener(eventType EventType, listener Listener) *typesListener {
	return NewTypesListener([]EventType{eventType}, listener)
}

func (l *typesListener) OnEvent(ctx context.Context, event *Event) error {
	eventType := event.Type
	if _, ok := l.eventType[eventType]; !ok {
		return nil
	}
	return l.listener.OnEvent(ctx, event)
}

type wrappedListener struct {
	listener Listener
}

func (l wrappedListener) OnEvent(ctx context.Context, event *Event) error {
	return l.listener.OnEvent(ctx, event)
}

func NewTopicAndTypeListener(eventTopic EventTopic, eventType EventType, listener Listener) *wrappedListener {
	return &wrappedListener{listener: NewTopicListener(eventTopic, NewTypeListener(eventType, listener))}
}

func NewTopicAndTypesListener(eventTopic EventTopic, eventType []EventType, listener Listener) *wrappedListener {
	var typeListener Listener
	if eventType != nil {
		typeListener = NewTypesListener(eventType, listener)
	} else {
		typeListener = listener
	}

	return &wrappedListener{listener: NewTopicListener(eventTopic, typeListener)}
}

func NewAdvancedTopicAndTypesListener(log *zap.Logger, eventTopic EventTopic, eventType []EventType, listener Listener) Listener {
	return NewTopicAndTypesListener(eventTopic, eventType, NewLoggerListener(log, NewPanicRecoveryListener(listener)))
}

type ListenerFanOut []Listener

func (ls ListenerFanOut) OnEvent(ctx context.Context, event *Event) error {
	for _, l := range ls {
		// FIXME: provide way to handle the error
		l.OnEvent(ctx, event)
	}
	return nil
}

func FanOutListeners(ls ...Listener) ListenerFanOut {
	return ls
}

type GoListener struct {
	Listener Listener
}

func NewGoListener(listener Listener) *GoListener {
	return &GoListener{Listener: listener}
}

func (ls *GoListener) OnEvent(ctx context.Context, event *Event) error {
	if ls.Listener != nil {
		// FIXME: provide way to handle the error
		go ls.Listener.OnEvent(ctx, event)
	}
	return nil
}

func GoFanOutListeners(listeners ...Listener) Listener {
	return &GoListener{
		Listener: FanOutListeners(listeners...),
	}
}

type ListenerFunc func(event *Event) error

func (l ListenerFunc) OnEvent(ctx context.Context, event *Event) error {
	return l(event)
}

type panicRecoveryListener struct {
	listener Listener
}

func NewPanicRecoveryListener(listener Listener) *panicRecoveryListener {
	return &panicRecoveryListener{listener: listener}
}

func (l panicRecoveryListener) OnEvent(ctx context.Context, event *Event) (err error) {
	defer func() {
		if r := recover(); r != nil {
			stack := zap.StackSkip("fake", 1).String
			err = fmt.Errorf("panic recovered: %v\n%s", r, stack)
		}
	}()
	err = l.listener.OnEvent(ctx, event)
	return err
}

type loggerListener struct {
	log      *zap.Logger
	Listener Listener
}

func NewLoggerListener(log *zap.Logger, listener Listener) *loggerListener {
	return &loggerListener{log: log, Listener: listener}
}

func (l loggerListener) OnEvent(ctx context.Context, event *Event) error {
	startTime := time.Now().UTC()

	log := l.log.With(
		zap.Stringer("topic", event.Topic),
		zap.Stringer("type", event.Type),
		zap.String("id", event.ID),
	)

	log.Info("event triggered")
	err := l.Listener.OnEvent(ctx, event)
	took := time.Now().UTC().Sub(startTime)
	if err != nil {
		log.Error("event failed", zap.Error(err), zap.Duration("took", took))
	} else {
		log.Info("event succeeded", zap.Duration("took", took))
	}
	return err
}
