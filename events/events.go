package events

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
)

const (
	eventIdSize = 10
)

type (
	Event struct {
		ID      string          `json:"id"`
		Topic   EventTopic      `json:"topic"`
		Type    EventType       `json:"action"`
		Payload json.RawMessage `json:"payload"`
	}

	Emitter interface {
		RegisterListeners(listeners ...Listener)
		Emit(ctx context.Context, event *Event) error
	}

	EventTopic string
	EventType  string
	Listener   interface {
		OnEvent(ctx context.Context, event *Event) error
	}
)

func (t EventTopic) String() string {
	return string(t)
}
func (a EventType) String() string {
	return string(a)
}

func NewEvent(
	topic EventTopic,
	eventType EventType,
	payload interface{},
) (*Event, error) {
	pj, err := json.Marshal(payload)
	if err != nil {
		return nil, err
	}
	return &Event{
		ID:      NewEventId(),
		Topic:   topic,
		Type:    eventType,
		Payload: pj,
	}, nil
}

func NewEventId() string {
	id := make([]byte, eventIdSize)
	_, err := rand.Read(id)
	if err != nil {
		panic(err)
	}
	return base64.RawURLEncoding.EncodeToString(id)
}

func (e *Event) GetPayload(out interface{}) error {
	return json.Unmarshal(e.Payload, out)
}

// +++++ Emitters helpers

type emitter struct {
	l ListenerFanOut
}

func NewEmitter() *emitter {
	return &emitter{}
}

func (e *emitter) RegisterListeners(listeners ...Listener) {
	e.l = append(e.l, listeners...)
}

func (e *emitter) Emit(ctx context.Context, event *Event) error {
	return e.l.OnEvent(ctx, event)
}

type goEmitter struct {
	*emitter
}

func NewGoEmitter() *goEmitter {
	return &goEmitter{emitter: NewEmitter()}
}

func (e *goEmitter) RegisterListeners(listeners ...Listener) {
	for _, listener := range listeners {
		e.l = append(e.l, NewGoListener(listener))
	}
}

type registerer interface {
	RegisterEventListeners(emitter Emitter)
}

func RegisterAll(e Emitter, registerer ...registerer) {
	for _, r := range registerer {
		r.RegisterEventListeners(e)
	}
}
