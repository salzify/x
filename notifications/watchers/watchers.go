package watchers

import (
	"context"
	"encoding/json"
	"strings"
	"sync"

	"go.uber.org/zap"

	"go.e.salzify.com/x/notifications"
)

type WatcherEntity string

func (e WatcherEntity) String() string {
	return string(e)
}

type Provider interface {
	Watch(ctx context.Context, entity WatcherEntity, entityId string, action string, watchersIds []string) error
	Watchers(ctx context.Context, entity WatcherEntity, entityId string, action string, excludeIds []string) ([]string, error)
}

type inMemoryProvider struct {
	m sync.Map
}

func InMemoryProvider() *inMemoryProvider {
	return &inMemoryProvider{}
}

func (p *inMemoryProvider) key(entity WatcherEntity, entityId string, action string) string {
	return strings.Join([]string{entity.String(), entityId, action}, "/")
}

func (p *inMemoryProvider) Watch(entity WatcherEntity, entityId string, action string, watchersIds []string) error {
	p.m.Store(p.key(entity, entityId, action), watchersIds)
	return nil
}

func (p *inMemoryProvider) Watchers(entity WatcherEntity, entityId string, action string, execlude []string) ([]string, error) {
	v, ok := p.m.Load(p.key(entity, entityId, action))
	if !ok {
		return nil, nil
	}
	ws := v.([]string)
	filtered := ws
	if len(execlude) > 0 {
		filtered = nil
		for _, e := range execlude {
			for _, w := range ws {
				if w != e {
					filtered = append(filtered, w)
				}
			}
		}

	}
	return filtered, nil
}

type WatcherNotifiable struct {
	// related watcher entity for the fired events.Event Topic and Action
	Entity WatcherEntity
	// mapped events.Event Payload to the related entity data
	EntityId string
	Action   string
	// auto watchers for this entity based on the events.Event Topic and Action
	AutoWatchersIds []string
	// AutoWatchersFromEntities copies watchers from the specified entities:entitiesIds pairs
	AutoWatchersFromEntities map[WatcherEntity][]string
	// excluded watchers won't be sent this notification
	ExcludedWatchersIds []string
}

func (*WatcherNotifiable) NewNotifiable(jsonIn []byte) notifications.Notifiable {
	n := WatcherNotifiable{}
	err := json.Unmarshal(jsonIn, &n)
	if err != nil {
		panic(err)
	}
	return &n
}

func (n *WatcherNotifiable) Nil() notifications.Notifiable {
	return nil
}

func (n *WatcherNotifiable) ID() notifications.NotifiableId {
	if n == nil {
		return ""
	}
	return notifications.NotifiableId(strings.Join([]string{n.Entity.String(), n.EntityId}, "/"))
}

func (n *WatcherNotifiable) String() string {
	return string(n.ID())
}

func (n *WatcherNotifiable) GoString() string {
	return n.String()
}

type NotifierData struct {
	// the template group to render for this notification
	TemplateGroup notifications.TemplateGroup
	Data          interface{}
}

type Notifier struct {
	log              *zap.Logger
	notifier         notifications.Notifier
	watchersProvider Provider
}

func NewNotifier(
	log *zap.Logger,
	notifier notifications.Notifier,
	watchersProvider Provider,
) *Notifier {
	return &Notifier{
		log:              log,
		notifier:         notifier,
		watchersProvider: watchersProvider,
	}
}

func (n *Notifier) Notify(ctx context.Context, options *notifications.NotificationOptions) error {
	groupName := options.TemplateGroup
	notifiables := options.Notifiables
	data := options.Data

	for _, notifiable := range notifiables {

		wn := notifiable.(*WatcherNotifiable)

		entity := wn.Entity
		entityId := wn.EntityId
		action := wn.Action

		log := n.log.With(
			zap.Stringer("entity", entity),
			zap.String("entity-id", entityId),
			zap.Stringer("template-group", groupName),
		)

		autoWatchersIds := wn.AutoWatchersIds
		for e, eIds := range wn.AutoWatchersFromEntities {
			for _, eId := range eIds {
				watchersIds, err := n.watchersProvider.Watchers(ctx, e, eId, action, nil)
				if err != nil {
					log.Error("unable to get watchers", zap.Error(err))
					return err
				}

				autoWatchersIds = append(autoWatchersIds, watchersIds...)
			}
		}

		autoWatchersIds = uniqueStrings(autoWatchersIds)
		excludedWatchersIds := uniqueStrings(wn.ExcludedWatchersIds)

		log = log.With(
			zap.Strings("auto-watchers-ids", autoWatchersIds),
			zap.Strings("excluded-watchers-ids", excludedWatchersIds),
		)

		if len(autoWatchersIds) > 0 {
			err := n.watchersProvider.Watch(ctx, entity, entityId, action, autoWatchersIds)
			if err != nil {
				log.Error(
					"unable to auo watch entity",
					zap.Error(err),
				)
				return err
			}
		}

		// get watchers for this topic, action, and entity-id
		watchersIds, err := n.watchersProvider.Watchers(ctx, entity, entityId, action, excludedWatchersIds)
		if err != nil {
			log.Error("unable to get watchers", zap.Error(err))
			return err
		}

		// watchersIds are the same as the notifiables ids
		notifiablesIds := notifications.NotifiableIdsFromStrings(watchersIds)

		err = n.notifier.Notify(ctx, &notifications.NotificationOptions{
			NotificationId: options.NotificationId,
			SourceId:       options.SourceId,
			TemplateGroup:  groupName,
			Actor:          options.Actor,
			Notifiables:    notifications.NotifiablesFromNotifiableIds(notifiablesIds),
			Data:           data,
		})
		if err != nil {
			log.Error(
				"unable to notify notifiables",
				zap.Strings("watchersIds", watchersIds),
				zap.Error(err),
			)
		}

	}
	return nil
}

func indexStrings(a []string) map[string]struct{} {
	m := map[string]struct{}{}
	for _, k := range a {
		m[k] = struct{}{}
	}
	return m
}

func mapKeys(m map[string]struct{}) []string {
	keys := make([]string, len(m), len(m))
	i := 0
	for k, _ := range m {
		keys[i] = k
		i++
	}
	return keys
}

func uniqueStrings(a []string) []string {
	return mapKeys(indexStrings(a))
}
