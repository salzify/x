package renderers

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"os"
	"path"
	"sync"
	textTemplate "text/template"

	"github.com/pkg/errors"
	"go.uber.org/zap"

	"go.e.salzify.com/x/notifications"
)

type templateDefinition struct {
	notifications.Def
	Template *textTemplate.Template
}

type contextData struct {
	NotificationId string
	SourceId       string
	Data           interface{}
	Extra          interface{}
	Actor          notifications.Actor
	Notifiable     notifications.Notifiable
}

type GoTemplateRenderer struct {
	logger *zap.Logger

	baseTemplatesPath string

	funcMap textTemplate.FuncMap

	mu                    sync.Mutex
	registeredDefinitions map[notifications.TemplateGroup]map[notifications.ChannelName]*templateDefinition
}

func NewGoTemplatesRender(
	logger *zap.Logger,
	baseTemplatesPath string,
	funcMap textTemplate.FuncMap,
) *GoTemplateRenderer {
	return &GoTemplateRenderer{
		logger: logger,

		baseTemplatesPath: path.Clean(baseTemplatesPath),

		funcMap: funcMap,

		registeredDefinitions: map[notifications.TemplateGroup]map[notifications.ChannelName]*templateDefinition{},
	}
}

// TODO: move the templates storage outside of Renderer responsibility
// This is tricky as each Renderer can optimize the templates definitions before
// the rendering process. Eg, GoTemplateRenderer parses the template files into memory
// for faster rendering.
func (m *GoTemplateRenderer) registerFSDefinition(
	def notifications.Def,
) error {
	fsPath := path.Join(m.baseTemplatesPath, def.Path)
	info, err := os.Stat(fsPath)
	if err != nil {
		return errors.Wrap(err, "unable to stat path")
	}
	var name, glob string
	if info.IsDir() {
		name = fmt.Sprintf("%s.gojson", path.Base(fsPath))
		glob = fmt.Sprintf("%s/**", fsPath)
	} else {
		name = path.Base(fsPath)
		glob = fsPath
	}

	template, err := newTemplate(name, m.funcMap).ParseGlob(glob)
	if err != nil {
		return err
	}

	return m.registerDefinition(
		def,
		template,
	)
}

func (m *GoTemplateRenderer) registerDefinition(
	def notifications.Def,
	template *textTemplate.Template,
) error {
	m.mu.Lock()
	defer m.mu.Unlock()

	groupName := def.GroupName
	channel := def.Channel

	var group map[notifications.ChannelName]*templateDefinition

	group, ok := m.registeredDefinitions[groupName]
	if !ok {
		group = map[notifications.ChannelName]*templateDefinition{}
		m.registeredDefinitions[groupName] = group
	}

	if _, ok := group[channel]; ok {
		//	already registered!
		return fmt.Errorf("a template for this group and channel is already registered")
	}

	group[channel] = &templateDefinition{
		Def:      def,
		Template: template,
	}

	return nil
}

func (m *GoTemplateRenderer) RegisterDefinitions(
	defs ...*notifications.Def,
) error {
	for _, def := range defs {
		if def.Renderer != nil {
			err := m.registerDefinition(*def, nil)
			if err != nil {
				return err
			}
		} else {
			err := m.registerFSDefinition(*def)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (m *GoTemplateRenderer) Render(ctx context.Context, options notifications.RenderOptions) (*notifications.Notification, error) {
	groupName := options.TemplateGroup
	channel := options.ChannelName
	actor := options.Actor
	notifiable := options.Notifiable
	data := options.Data

	m.mu.Lock()
	defer m.mu.Unlock()

	gdefs, ok := m.registeredDefinitions[groupName]
	if !ok {
		return nil, fmt.Errorf("no registered definitions for group")
	}

	def, ok := gdefs[channel]
	if !ok {
		return nil, fmt.Errorf("no registered definitions for channel")
	}

	if def.Renderer != nil {
		// custom Renderer
		return def.Renderer.Render(ctx, options)
	}

	contextData := contextData{
		NotificationId: options.NotificationId,
		SourceId:       options.SourceId,
		Data:           data,
		Extra:          def.ExtraData,
		Actor:          actor,
		Notifiable:     notifiable,
	}

	payload, err := renderTemplate(def.Template, contextData)
	if err != nil {
		return nil, errors.Wrap(err, "unable to render template")
	}
	return &notifications.Notification{
		Actor:      actor,
		Notifiable: notifiable,
		Payload:    payload,
		Channel:    channel,
	}, nil
}

func newTemplate(name string, funcMap textTemplate.FuncMap) *textTemplate.Template {
	template := textTemplate.New(name)

	template.Funcs(textTemplate.FuncMap{
		"template_json": func(name string, dot interface{}) (string, error) {
			t := template.Lookup(name)
			if t == nil {
				return "", fmt.Errorf("%s template not found", name)
			}

			contextData := dot.(contextData) // will panic if fails
			rendered, err := renderTemplate(t, contextData)
			if err != nil {
				return "", err
			}

			out, err := json.Marshal(string(rendered))
			if err != nil {
				return "", err
			}

			out = bytes.Trim(out, "\"")

			return string(out), nil
		},
	})

	template.Funcs(funcMap)

	return template
}

func renderTemplate(t *textTemplate.Template, contextData contextData) ([]byte, error) {

	buffer := bytes.Buffer{}

	err := t.Execute(&buffer, contextData)
	if err != nil {
		return nil, err
	}

	return buffer.Bytes(), nil
}
