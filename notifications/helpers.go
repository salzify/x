package notifications

func NotifiableIdsFromStrings(ids []string) []NotifiableId {
	nids := make([]NotifiableId, len(ids), len(ids))
	for i, id := range ids {
		nids[i] = NotifiableId(id)
	}
	return nids
}

func NotifiableIdsFromNotifiable(notifiables []Notifiable) []NotifiableId {
	nids := make([]NotifiableId, len(notifiables), len(notifiables))
	for i, n := range notifiables {
		nids[i] = n.ID()
	}
	return nids
}

func NotifiablesFromNotifiableIds(ids []NotifiableId) []Notifiable {
	ns := make([]Notifiable, len(ids), len(ids))
	for i, id := range ids {
		n := id
		ns[i] = &IDNotifiable{NotifiableId: n}
	}
	return ns
}
