package notifications

import (
	"context"
)

type ChannelName string

func (cn ChannelName) String() string {
	return string(cn)
}

type TemplateGroup string

func (t TemplateGroup) String() string {
	return string(t)
}

type NotificationOptions struct {
	NotificationId string
	SourceId       string
	TemplateGroup  TemplateGroup
	Actor          Actor
	Notifiables    []Notifiable
	Data           interface{}
}

type Notification struct {
	Id         string
	SourceId   string
	Actor      Actor
	Notifiable Notifiable
	Channel    ChannelName
	Payload    []byte
	Meta       []byte
}

type Def struct {
	GroupName TemplateGroup
	Channel   ChannelName
	// Path relative path of the template file
	Path string
	// TODO: Templates hash of template name and raw template. If specified, Path is ignored.
	//Templates map[string]string
	// ExtraData is a static data to pass to this template.
	ExtraData interface{}
	// Custom Renderer function to be used for this Def only
	Renderer Renderer
}

type RenderOptions struct {
	NotificationId string
	SourceId       string
	TemplateGroup  TemplateGroup
	ChannelName    ChannelName
	// TODO: Def overrides the used def to render. If specified, TemplateGroup and ChannelName are ignored.
	//Def        *Def
	Actor      Actor
	Notifiable Notifiable
	Data       interface{}
}

type Notifier interface {
	Notify(ctx context.Context, options *NotificationOptions) error
}

type Channel interface {
	Notify(ctx context.Context, notification *Notification) error
	Name() ChannelName

	SetEnabled(enabled bool)
	Enabled() bool

	StopOnFail() bool
}

type Renderer interface {
	RegisterDefinitions(defs ...*Def) error
	Render(ctx context.Context, options RenderOptions) (*Notification, error)
}

type RenderFunc func(ctx context.Context, moptions RenderOptions) (*Notification, error)

func (f RenderFunc) RegisterDefinitions(defs ...*Def) error {
	panic("RenderFunc don't support registering definitions")
}

func (f RenderFunc) Render(ctx context.Context, options RenderOptions) (*Notification, error) {
	return f(ctx, options)
}
