package notifications

import "fmt"

type ActorId string

const (
	EmptyActorID ActorId = ""
)

func (id *ActorId) String() string {
	if id == nil {
		return string(EmptyActorID)
	}
	return string(*id)
}

func (id *ActorId) GoString() string {
	return id.String()
}

type Actor interface {
	fmt.Stringer
	fmt.GoStringer
	NewActor(jsonIn []byte) Actor
	ID() ActorId
}

type IDActor struct {
	ActorId
}

func (u *IDActor) NewActor(jsonIn []byte) Actor {
	nId := IDActor{ActorId: ActorId(jsonIn)} // jsonIn should be just a string!
	return &nId
}

func (u *IDActor) ID() ActorId {
	if u == nil {
		return EmptyActorID
	}
	return u.ActorId
}
