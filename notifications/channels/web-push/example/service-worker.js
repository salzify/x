self.addEventListener('push', event => {
    console.log('[Service Worker] Push Received.');
    console.log(event);
    console.log(`[Service Worker] Push had this data: "${event.data.text()}"`);

    data = event.data.json()
    notification = data.notification

    const title = notification.title;
    const options = notification;

    event.waitUntil(self.registration.showNotification(title, options));
});
