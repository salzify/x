package webpush

import (
	"context"
	"encoding/json"

	"github.com/SherClockHolmes/webpush-go"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"

	"go.e.salzify.com/x/notifications"
	"go.e.salzify.com/x/notifications/channels"
)

const (
	ChannelNameWebPush notifications.ChannelName = "web-push"
	requestTtl                                   = 30
)

type WebPushNotifiable interface {
	WebPushSubscriptions() []*IdentifiedSubscription
}

type WebPushPayload struct {
	Notification Notification `json:"notification"`
	Topic        string       `json:"topic,omitempty"`
	Urgency      Urgency      `json:"urgency,omitempty"`
}

type WebPush struct {
	*channels.ChannelBase
	log *zap.Logger

	vapidPublicKey  string
	vapidPrivateKey string
	vapidSubject    string
}

type PushEventData struct {
	Notification Notification `json:"notification"`
}

func NewWebPush(log *zap.Logger, vapidPublicKey, vapidPrivateKey, vapidSubject string) *WebPush {
	return &WebPush{
		ChannelBase: channels.NewChannelBase(),

		log: log,

		vapidPublicKey:  vapidPublicKey,
		vapidPrivateKey: vapidPrivateKey,
		vapidSubject:    vapidSubject,
	}
}

func (wp *WebPush) Notify(ctx context.Context, notification *notifications.Notification) error {
	if !wp.Enabled() {
		return nil
	}

	wpn, ok := notification.Notifiable.(WebPushNotifiable)
	if !ok {
		return channels.InvalidNotifiableTypeErr
	}

	// TODO: validate payload
	var payload WebPushPayload
	err := json.Unmarshal(notification.Payload, &payload)
	if err != nil {
		return err
	}

	message, err := json.Marshal(PushEventData{
		Notification: payload.Notification,
	})
	if err != nil {
		return err
	}

	// TODO: validate subscription data
	subscriptions := wpn.WebPushSubscriptions()

	eg := errgroup.Group{}

	for _, s := range subscriptions {
		func(subscription *IdentifiedSubscription) {
			eg.Go(func() error {
				// Send Notification
				resp, err := webpush.SendNotification(
					message,
					&webpush.Subscription{
						Endpoint: subscription.Endpoint,
						Keys: webpush.Keys{
							Auth:   subscription.Keys.Auth,
							P256dh: subscription.Keys.P256dh,
						},
					},
					&webpush.Options{
						Subscriber:      wp.vapidSubject, // Do not include "mailto:"
						VAPIDPublicKey:  wp.vapidPublicKey,
						VAPIDPrivateKey: wp.vapidPrivateKey,
						TTL:             requestTtl,
						Topic:           payload.Topic,
						Urgency:         webpush.Urgency(payload.Urgency),
					})
				if err != nil {
					wp.log.Error(
						"unable to send notification for subscription",
						zap.String("subscription-id", subscription.Id),
					)
					return nil // swallow the error so errgroup don't stop
				}
				_ = resp.Body.Close()
				return nil
			})
		}(s)

	}

	return eg.Wait() // will always return nil
}

func (wp *WebPush) Name() notifications.ChannelName {
	return ChannelNameWebPush
}
