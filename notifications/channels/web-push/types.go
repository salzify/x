package webpush

// Keys are the base64 encoded values from PushSubscription.getKey()
type Keys struct {
	Auth   string `json:"auth"`
	P256dh string `json:"p256dh"`
}

// Subscription represents a PushSubscription object from the Push API
type Subscription struct {
	Endpoint string `json:"endpoint"`
	Keys     Keys   `json:"keys"`
}

// IdentifiedSubscription represents a PushSubscription object with server-specific unique id
type IdentifiedSubscription struct {
	Subscription
	Id string `json:"id"`
}

// Urgency indicates to the push service how important a message is to the user.
// This can be used by the push service to help conserve the battery life of a user's device
// by only waking up for important messages when battery is low.
type Urgency string

const (
	// UrgencyVeryLow requires device state: on power and Wi-Fi
	UrgencyVeryLow Urgency = "very-low"
	// UrgencyLow requires device state: on either power or Wi-Fi
	UrgencyLow Urgency = "low"
	// UrgencyNormal excludes device state: low battery
	UrgencyNormal Urgency = "normal"
	// UrgencyHigh admits device state: low battery
	UrgencyHigh Urgency = "high"
)

type NotificationDirection string

const (
	NotificationDirectionAuto NotificationDirection = "auto"
	NotificationDirectionLtr  NotificationDirection = "ltr"
	NotificationDirectionRtl  NotificationDirection = "rtl"
)

type NotificationAction struct {
	Action string `json:"action,omitempty"`
	Title  string `json:"title,omitempty"`
	Icon   string `json:"icon,omitempty"`
}

type Notification struct {
	Title              string                `json:"title,omitempty"`
	Dir                NotificationDirection `json:"dir,omitempty"`
	Lang               string                `json:"lang,omitempty"`
	Body               string                `json:"body,omitempty"`
	Tag                string                `json:"tag,omitempty"`
	Image              string                `json:"image,omitempty"`
	Icon               string                `json:"icon,omitempty"`
	Badge              string                `json:"badge,omitempty"`
	Sound              string                `json:"sound,omitempty"`
	Vibrate            []int                 `json:"vibrate,omitempty"`
	Timestamp          int                   `json:"timestamp,omitempty"`
	Renotify           bool                  `json:"renotify,omitempty"`
	Silent             bool                  `json:"silent,omitempty"`
	RequireInteraction bool                  `json:"require_interaction,omitempty"`
	Data               interface{}           `json:"data,omitempty"`
	Actions            []NotificationAction  `json:"actions,omitempty"`
}
