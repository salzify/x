package mail

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"go.e.salzify.com/x/notifications"
	"go.e.salzify.com/x/notifications/channels"
)

const (
	ChannelNameMailer notifications.ChannelName = "mail"
)

type EmailNotifiable interface {
	GetName() string
	GetEmail() string
}

type Address struct {
	Name  string
	Email string
}

func (a *Address) GetName() string {
	return a.Name
}
func (a *Address) GetEmail() string {
	return a.Email
}

type Mailer interface {
	SendMail(
		from EmailNotifiable,
		to []EmailNotifiable,
		cc []EmailNotifiable,
		bcc []EmailNotifiable,
		replyTo EmailNotifiable,
		subject string,
		textBody *string,
		htmlBody *string,
		headers map[string][]string,
		dateHeaders map[string]time.Time,
	) error
}

type MailerChannel struct {
	*channels.ChannelBase

	mailer Mailer
}

func NewMailerChannel(
	mailer Mailer,
) *MailerChannel {
	return &MailerChannel{
		ChannelBase: channels.NewChannelBase(),
		mailer:      mailer,
	}
}

type MailerPayload struct {
	From    *Address  `json:"from,omitempty"`
	To      []Address `json:"to,omitempty"`
	Cc      []Address `json:"cc,omitempty"`
	Bcc     []Address `json:"bcc,omitempty"`
	ReplyTo *Address  `json:"reply_to"`
	Subject string    `json:"subject,omitempty"`
	Text    *string   `json:"text,omitempty"`
	Html    *string   `json:"html,omitempty"`

	Headers     map[string][]string  `json:"headers,omitempty"`
	DateHeaders map[string]time.Time `json:"date_headers,omitempty"`
}

func (c *MailerChannel) Notify(ctx context.Context, notification *notifications.Notification) error {
	if !c.Enabled() {
		return nil
	}
	var payload MailerPayload
	err := json.Unmarshal(notification.Payload, &payload)
	if err != nil {
		return err
	}

	if len(payload.To) == 0 {
		// auto use notifiable only if there is no To header specified from the payload
		notifiable, ok := notification.Notifiable.(EmailNotifiable)
		if !ok {
			return fmt.Errorf("unable to cast Notifiable to EmailNotifiable; acutalType: %T", notifiable)
		}
		payload.To = []Address{
			{
				Name:  notifiable.GetName(),
				Email: notifiable.GetEmail(),
			},
		}
	}

	return c.mailer.SendMail(
		payload.From,
		addressesToNotifiable(payload.To),
		addressesToNotifiable(payload.Cc),
		addressesToNotifiable(payload.Bcc),
		payload.ReplyTo,
		payload.Subject,
		payload.Text,
		payload.Html,
		payload.Headers,
		payload.DateHeaders,
	)
}

func (c *MailerChannel) Name() notifications.ChannelName {
	return ChannelNameMailer
}

func addressesToNotifiable(addresses []Address) []EmailNotifiable {
	res := make([]EmailNotifiable, len(addresses))
	for i, a := range addresses {
		res[i] = &a
	}
	return res
}
