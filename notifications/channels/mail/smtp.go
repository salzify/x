package mail

import (
	"fmt"
	"reflect"
	"time"

	"gopkg.in/gomail.v2"
)

type SMTPMailer struct {
	dialer        *gomail.Dialer
	defaultSender EmailNotifiable
}

func formatAddress(a EmailNotifiable, msg *gomail.Message) string {
	return msg.FormatAddress(a.GetEmail(), a.GetName())
}

func NewSMTPMailer(
	host string,
	port int,
	username, password string,
	localName string,
	defaultSender EmailNotifiable,
) *SMTPMailer {
	dialer := gomail.NewDialer(
		host,
		port,
		username,
		password,
	)
	dialer.LocalName = localName

	return &SMTPMailer{
		dialer:        dialer,
		defaultSender: defaultSender,
	}
}

func (m *SMTPMailer) SendMail(
	from EmailNotifiable,
	to []EmailNotifiable,
	cc []EmailNotifiable,
	bcc []EmailNotifiable,
	replyTo EmailNotifiable,
	subject string,
	textBody *string,
	htmlBody *string,
	headers map[string][]string,
	dateHeaders map[string]time.Time,
) error {
	if len(to) == 0 {
		return fmt.Errorf("to is required")
	}

	if textBody == nil && htmlBody == nil {
		return fmt.Errorf("either textBody or htmlBody or both should be specirifed")
	}

	msg := gomail.NewMessage()

	if isNil(from) {
		from = m.defaultSender
	}

	msg.SetHeader("From", formatAddress(from, msg))
	msg.SetHeader("To", formatAddresses(to, msg)...)
	msg.SetHeader("Cc", formatAddresses(cc, msg)...)
	msg.SetHeader("Bcc", formatAddresses(bcc, msg)...)

	if !isNil(replyTo) {
		msg.SetHeader("Reply-To", formatAddress(replyTo, msg))
	}

	msg.SetHeader("Subject", subject)

	msg.SetHeaders(headers)
	for k, v := range dateHeaders {
		msg.SetDateHeader(k, v)
	}

	if textBody != nil {
		msg.SetBody("text/plain", *textBody)
	}
	if htmlBody != nil {
		msg.AddAlternative("text/html", *htmlBody)
	}

	return m.dialer.DialAndSend(msg)
}

func formatAddresses(users []EmailNotifiable, msg *gomail.Message) []string {
	toStr := make([]string, len(users))
	for i, t := range users {
		toStr[i] = formatAddress(t, msg)
	}
	return toStr
}

func isNil(i interface{}) bool {
	if i == nil {
		return true
	}
	switch reflect.TypeOf(i).Kind() {
	case reflect.Ptr, reflect.Map, reflect.Array, reflect.Chan, reflect.Slice:
		return reflect.ValueOf(i).IsNil()
	}
	return false
}
