package fcmpush

import (
	"context"
	"encoding/json"

	"firebase.google.com/go/v4/messaging"
	"go.uber.org/zap"

	"go.e.salzify.com/x/notifications"
	"go.e.salzify.com/x/notifications/channels"
)

const (
	ChannelNameFCMPush notifications.ChannelName = "fcm-push"
)

type PushPlatform string

func (p PushPlatform) String() string {
	return string(p)
}

const (
	PushPlatformWebpush PushPlatform = "webpush"
	PushPlatformAndroid PushPlatform = "android"
	PushPlatformAPNS    PushPlatform = "apns"
)

type Subscription struct {
	Token    string       `json:"token"`
	Platform PushPlatform `json:"platform"`
}

type FCMPushNotifiable interface {
	FCMPushSubscriptions() []Subscription
}

type Payload struct {
	Data         map[string]string        `json:"data"`
	Notification *messaging.Notification  `json:"notification"`
	Android      *messaging.AndroidConfig `json:"android"`
	Webpush      *messaging.WebpushConfig `json:"webpush"`
	APNS         *messaging.APNSConfig    `json:"APNS"`
}

type FCMPush struct {
	*channels.ChannelBase
	log *zap.Logger
	c   *messaging.Client
}

func NewFCMPush(log *zap.Logger, c *messaging.Client) *FCMPush {
	return &FCMPush{
		ChannelBase: channels.NewChannelBase(),
		log:         log,
		c:           c,
	}
}

func (p *FCMPush) Notify(ctx context.Context, notification *notifications.Notification) error {
	if !p.Enabled() {
		return nil
	}

	notifiable, ok := notification.Notifiable.(FCMPushNotifiable)
	if !ok {
		return channels.InvalidNotifiableTypeErr
	}

	// TODO: validate payload
	var payload Payload
	err := json.Unmarshal(notification.Payload, &payload)
	if err != nil {
		return err
	}

	subs := notifiable.FCMPushSubscriptions()
	if len(subs) == 0 {
		// no fcp subscriptions, ignore
		return nil
	}

	messages := make([]*messaging.Message, len(subs), len(subs))
	for i, sub := range subs {
		messages[i] = &messaging.Message{
			Data:         payload.Data,
			Notification: payload.Notification,
			Android:      payload.Android,
			Webpush:      payload.Webpush,
			APNS:         payload.APNS,
			FCMOptions:   nil,
			Token:        sub.Token,
			Topic:        "",
			Condition:    "",
		}
	}

	resp, err := p.c.SendEach(ctx, messages)
	if err != nil {
		return err
	}
	if resp.FailureCount > 0 {
		for _, response := range resp.Responses {
			if !response.Success {
				p.log.Error("failed to send FCM push message", zap.Error(response.Error))
			}
		}
	}
	return nil
}

func (p *FCMPush) Name() notifications.ChannelName {
	return ChannelNameFCMPush
}
