package inapp

import "time"

type NotificationPayload struct {
	Id        string      `json:"id,omitempty"`
	SourceId  string      `json:"source_id,omitempty"`
	Title     string      `json:"title,omitempty"`
	Body      string      `json:"body,omitempty"`
	Image     *string     `json:"image,omitempty"`
	Timestamp time.Time   `json:"timestamp,omitempty"`
	Data      interface{} `json:"data,omitempty"`
}
