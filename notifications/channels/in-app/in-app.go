package inapp

import (
	"context"
	"encoding/json"

	"go.e.salzify.com/x/notifications"
	"go.e.salzify.com/x/notifications/channels"
)

const (
	ChannelNameInApp notifications.ChannelName = "in-app"
)

type sender interface {
	SendNotification(ctx context.Context, actorId string, userId string, notification NotificationPayload) error
}

type inAppChannel struct {
	*channels.ChannelBase

	sender sender
}

func NewInAppChannel(
	sender sender,
) *inAppChannel {
	return &inAppChannel{
		ChannelBase: channels.NewChannelBase(),

		sender: sender,
	}
}

func (c *inAppChannel) Notify(ctx context.Context, notification *notifications.Notification) error {
	if !c.Enabled() {
		return nil
	}
	var payload NotificationPayload
	err := json.Unmarshal(notification.Payload, &payload)
	if err != nil {
		return err
	}

	// TODO: validate

	actorId := notification.Actor.ID()
	userId := notification.Notifiable.ID()

	return c.sender.SendNotification(ctx, actorId.String(), userId.String(), payload)
}

func (c *inAppChannel) Name() notifications.ChannelName {
	return ChannelNameInApp
}
