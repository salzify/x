package channels

import (
	"errors"
)

var (
	InvalidNotifiableTypeErr = errors.New("invalid notifiable type")
)

type ChannelBase struct {
	enabled    bool
	stopOnFail bool
}

func NewChannelBase() *ChannelBase {
	return &ChannelBase{
		enabled:    true,
		stopOnFail: false,
	}
}

func (c *ChannelBase) SetEnabled(enabled bool) {
	c.enabled = enabled
}

func (c *ChannelBase) Enabled() bool {
	return c.enabled
}

func (c *ChannelBase) SetStopOnFail(stop bool) {
	c.stopOnFail = stop
}

func (c *ChannelBase) StopOnFail() bool {
	return c.stopOnFail
}
