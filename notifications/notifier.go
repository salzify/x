package notifications

import (
	"context"

	"github.com/gofrs/uuid/v5"
	"github.com/pkg/errors"
	"go.uber.org/zap"
)

type NotifiablesProvider interface {
	NotifiablesByIds(ctx context.Context, ids []NotifiableId) ([]Notifiable, error)
}

type MultiChannelNotifier struct {
	logger             *zap.Logger
	notifiableProvider NotifiablesProvider
	renderer           Renderer
	channels           []Channel
}

func NewMultiChannelNotifier(
	logger *zap.Logger,
	notifiableProvider NotifiablesProvider,
	renderer Renderer,
	channels ...Channel,
) *MultiChannelNotifier {
	n := &MultiChannelNotifier{
		logger:             logger,
		notifiableProvider: notifiableProvider,
		renderer:           renderer,
		channels:           channels,
	}
	return n
}

func (n *MultiChannelNotifier) RegisterChannels(channels ...Channel) {
	n.channels = append(n.channels, channels...)
}

func (n *MultiChannelNotifier) Notify(ctx context.Context, options *NotificationOptions) error {
	groupName := options.TemplateGroup
	notifiables := options.Notifiables
	data := options.Data

	if options.NotificationId == "" {
		options.NotificationId = uuid.Must(uuid.NewV4()).String()
	}

	if options.SourceId == "" {
		options.SourceId = uuid.Must(uuid.NewV4()).String()
	}

	notifiablesIds := NotifiableIdsFromNotifiable(notifiables)
	log := n.logger.With(
		zap.String("notification-id", options.NotificationId),
		zap.String("source-id", options.SourceId),
		zap.Stringer("group-name", groupName),
		zap.Reflect("notifiables-ids", notifiablesIds),
	)

	notifiables, err := n.notifiableProvider.NotifiablesByIds(ctx, notifiablesIds)
	if err != nil {
		log.Error("unable to get notifiables from ids")
		return err
	}

	for _, notifiable := range notifiables {
		notifiableId := notifiable.ID()
		for _, channel := range n.channels {
			log := log.With(
				zap.Stringer("channel", channel.Name()),
				zap.Stringer("notifiable-id", &notifiableId),
			)
			if !channel.Enabled() {
				log.Debug("skipping disabled channel")
				continue
			}
			err = n.notifyChannelNotifiable(ctx, options, channel, notifiable, data)
			if err != nil {
				log.Error("unable to notify notifiable through channel", zap.Error(err))
				if channel.StopOnFail() {
					log.Debug("stopOnFail is enabled for channel, stopping...")
					return err
				}
			}
		}
	}
	return nil
}

func (n *MultiChannelNotifier) notifyChannelNotifiable(
	ctx context.Context,
	options *NotificationOptions,
	channel Channel,
	notifiable Notifiable,
	data interface{},
) error {
	renderOptions := RenderOptions{
		NotificationId: options.NotificationId,
		SourceId:       options.SourceId,
		TemplateGroup:  options.TemplateGroup,
		ChannelName:    channel.Name(),
		Actor:          options.Actor,
		Notifiable:     notifiable,
		Data:           data,
	}
	notification, err := n.renderer.Render(ctx, renderOptions)
	if err != nil {
		return errors.Wrap(err, "unable to render notification")
	}
	err = channel.Notify(ctx, notification)
	if err != nil {
		return errors.Wrap(err, "unable to notify channel")
	}
	return nil
}
