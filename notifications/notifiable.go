package notifications

import "fmt"

type NotifiableId string

const (
	EmptyNotifiableID NotifiableId = ""
)

func (id *NotifiableId) String() string {
	if id == nil {
		return string(EmptyNotifiableID)
	}
	return string(*id)
}

func (id *NotifiableId) GoString() string {
	return id.String()
}

type Notifiable interface {
	fmt.Stringer
	fmt.GoStringer
	NewNotifiable(jsonIn []byte) Notifiable
	ID() NotifiableId
}

type IDNotifiable struct {
	NotifiableId
}

func (u *IDNotifiable) NewNotifiable(jsonIn []byte) Notifiable {
	nId := IDNotifiable{NotifiableId: NotifiableId(jsonIn)} // jsonIn should be just a string!
	return &nId
}

func (u *IDNotifiable) ID() NotifiableId {
	if u == nil {
		return EmptyNotifiableID
	}
	return u.NotifiableId
}
