package featureflags

type Schema struct {
	Requirements []Requirement
}

func NewSchema(requirements []Requirement) *Schema {
	return &Schema{Requirements: requirements}
}

type Requirement struct {
	Flags    []string
	MatchAny bool
}

func NewRequirement(flags []string, matchAny bool) *Requirement {
	return &Requirement{Flags: uniqueFlags(flags), MatchAny: matchAny}
}

func uniqueFlags(flags []string) []string {
	var out []string
	flagsIndex := map[string]struct{}{}
	appendFlag := func(f string) {
		if _, ok := flagsIndex[f]; !ok {
			flagsIndex[f] = struct{}{}
			out = append(out, f)
		}
	}

	for _, flag := range flags {
		appendFlag(flag)
	}

	return out
}
