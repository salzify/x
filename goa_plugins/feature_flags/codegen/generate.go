package codegen

import (
	"bytes"
	"fmt"
	"path/filepath"
	"strconv"
	"strings"
	"text/template"

	"goa.design/goa/v3/codegen"
	"goa.design/goa/v3/codegen/service"
	"goa.design/goa/v3/eval"

	featureflags "go.e.salzify.com/x/goa_plugins/feature_flags"
	"go.e.salzify.com/x/goa_plugins/feature_flags/expr"
)

// Register the plugin Generator functions.
func init() {
	codegen.RegisterPluginLast("feature_flags", "gen", nil, Generate)
}

// Generate generates the call to otelhttp.WithRouteTag
func Generate(genpkg string, roots []eval.Root, files []*codegen.File) ([]*codegen.File, error) {
	for _, f := range files {
		if filepath.Base(f.Path) == "service.go" {

			codegen.AddImport(
				f.SectionTemplates[0],
				&codegen.ImportSpec{Path: "go.e.salzify.com/x/goa_plugins/feature_flags", Name: "featureflags"},
			)

			f.SectionTemplates = append(
				f.SectionTemplates, &codegen.SectionTemplate{
					Name: "feature-flags-auther",
					Source: `
type FeatureFlagAuther interface {
	FeatureFlagAuth(ctx context.Context, schema *featureflags.Schema) error
}
`,
				},
			)
		}
		if filepath.Base(f.Path) == "endpoints.go" {

			codegen.AddImport(
				f.SectionTemplates[0],
				&codegen.ImportSpec{Path: "go.e.salzify.com/x/goa_plugins/feature_flags", Name: "featureflags"},
			)

			for _, s := range f.SectionTemplates {
				if s.Name == "endpoint-method" {
					data := s.Data.(*service.EndpointMethodData)
					schema := expr.FeatureFlags(data.ServiceName, data.Name)

					schemaInit, err := getSchemaInit(schema)
					if err != nil {
						return nil, err
					}
					s.Source = strings.Replace(
						s.Source,
						`) goa.Endpoint`,
						`, fgFunc func(ctx context.Context, schema *featureflags.Schema) error) goa.Endpoint`,
						1,
					)
					s.Source = strings.Replace(
						s.Source,
						`{{- if .ServerStream }}`,
						fmt.Sprintf(
							`
%s
if err := fgFunc(ctx, schema); err!=nil {
return nil, err
}
{{- if .ServerStream }}`,
							schemaInit,
						),
						1,
					)
				}

				if s.Name == "endpoints-init" {
					s.Source = strings.Replace(
						s.Source,
						"return &{{ .VarName }}{",
						`fgAuthorizeFunc := func(ctx context.Context, schema *featureflags.Schema) error {return nil}
if f, ok := s.(FeatureFlagAuther); ok {
	fgAuthorizeFunc = f.FeatureFlagAuth
}
return &{{ .VarName }}{`,
						1,
					)
					s.Source = strings.Replace(
						s.Source,
						"{{ .VarName }}: New{{ .VarName }}Endpoint(s{{ range .Schemes }}, a.{{ .Type }}Auth{{ end }}),",
						`{{ .VarName }}: New{{ .VarName }}Endpoint(s{{ range .Schemes }}, a.{{ .Type }}Auth{{ end }}, fgAuthorizeFunc),`,
						1,
					)
				}
			}
		}
	}
	return files, nil
}

func getSchemaInit(schema *featureflags.Schema) (string, error) {
	schemaInitTmpl := `
schema := &featureflags.Schema{
	Requirements: []featureflags.Requirement{
		{{-  range .Requirements }}
		*featureflags.NewRequirement( []string{ {{- format .Flags -}} } , {{- format .MatchAny -}}),
		{{ end -}}
	},
}
`
	format := func(v any) string {
		switch vv := v.(type) {
		case bool:
			return strconv.FormatBool(vv)
		case []string:
			if len(vv) == 0 {
				return ""
			}
			return `"` + strings.Join(vv, `", "`) + `"`
		default:
			return fmt.Sprintf("%v", vv)
		}
	}

	buf := bytes.NewBuffer(nil)
	err := template.Must(template.New("schema-init").Funcs(map[string]any{"format": format}).Parse(schemaInitTmpl)).Execute(buf, schema)
	if err != nil {
		return "", err
	}
	return buf.String(), nil
}
