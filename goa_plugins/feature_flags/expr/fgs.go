package expr

import (
	"fmt"

	"goa.design/goa/v3/eval"

	featureflags "go.e.salzify.com/x/goa_plugins/feature_flags"
)

// FeatureFlagExpr describes a CORS policy.
type FeatureFlagExpr struct {
	// Flags is the list of required feature flags names.
	Flags    []string
	MatchAny bool
	Override bool

	// Parent expression, ServiceExpr or MethodExpr.
	Parent eval.Expression
}

// EvalName returns the generic expression name used in error messages.
func (o *FeatureFlagExpr) EvalName() string {
	var suffix string
	if o.Parent != nil {
		suffix = fmt.Sprintf(" of %s", o.Parent.EvalName())
	}
	return "FeatureFlags" + suffix
}

// FeatureFlags returns the flags for the given method.
// dim 1 are ORed then dim 0 are ANDed
func FeatureFlags(svc, method string) *featureflags.Schema {
	var requirements []featureflags.Requirement
	appendExpr := func(expr *FeatureFlagExpr) {
		requirement := featureflags.NewRequirement(expr.Flags, expr.MatchAny)
		if expr.Override {
			requirements = []featureflags.Requirement{*requirement}
		} else {
			requirements = append(requirements, *requirement)
		}
	}

	for s, sfs := range Root.ServiceFlags {
		if s == svc {
			for _, sf := range sfs {
				appendExpr(sf)
			}
		}
	}
	for s, no := range Root.ServiceMethodFlags {
		if s == svc {
			for m, mfs := range no {
				if m == method {
					for _, mf := range mfs {
						appendExpr(mf)
					}
				}
			}
		}
	}

	return featureflags.NewSchema(requirements)
}
