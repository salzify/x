package expr

import (
	"goa.design/goa/v3/eval"
	"goa.design/goa/v3/expr"
)

// Root is the design root expression.
var Root = &RootExpr{
	ServiceFlags:       make(map[string][]*FeatureFlagExpr),
	ServiceMethodFlags: make(map[string]map[string][]*FeatureFlagExpr),
}

type (
	// RootExpr keeps track of the flags defined in the design.
	RootExpr struct {
		// ServiceFlags lists all the flags definitions indexed by service.
		ServiceFlags map[string][]*FeatureFlagExpr

		// ServiceMethodFlags lists all the flags definitions indexed by service and method.
		ServiceMethodFlags map[string]map[string][]*FeatureFlagExpr
	}
)

// Register design root with eval engine.
func init() {
	eval.Register(Root)
}

// EvalName returns the name used in error messages.
func (r *RootExpr) EvalName() string {
	return "FeatureFlags plugin"
}

// WalkSets iterates over the API-level and service-level definitions.
func (r *RootExpr) WalkSets(walk eval.SetWalker) {
	expressionSet := make(eval.ExpressionSet, 0, len(r.ServiceFlags))
	for _, flagExprs := range r.ServiceFlags {
		for _, flagExpr := range flagExprs {
			expressionSet = append(expressionSet, flagExpr)
		}
	}
	walk(expressionSet)
	expressionSet = make(eval.ExpressionSet, 0, len(r.ServiceMethodFlags))
	for _, m := range r.ServiceMethodFlags {
		for _, flagExprs := range m {
			for _, flagExpr := range flagExprs {
				expressionSet = append(expressionSet, flagExpr)
			}
		}
	}
	walk(expressionSet)
}

// DependsOn tells the eval engine to run the goa DSL first.
func (r *RootExpr) DependsOn() []eval.Root {
	return []eval.Root{expr.Root}
}

// Packages returns the import path to the Go packages that make
// up the DSL. This is used to skip frames that point to files
// in these packages when computing the location of errors.
func (r *RootExpr) Packages() []string {
	return []string{"go.e.salzify.com/x/goa_plugins/feature_flags/dsl"}
}
