package dsl

import (
	"goa.design/goa/v3/eval"
	goaexpr "goa.design/goa/v3/expr"

	"go.e.salzify.com/x/goa_plugins/feature_flags/expr"
)

func FeatureFlags(name string, names ...string) {
	currentExpr := eval.Current()
	fge := &expr.FeatureFlagExpr{
		Flags:    append([]string{name}, names...),
		MatchAny: false,
		Override: false,
		Parent:   currentExpr,
	}

	featureFlags(fge)
}

func FeatureFlagsAny(name string, names ...string) {
	currentExpr := eval.Current()
	fge := &expr.FeatureFlagExpr{
		Flags:    append([]string{name}, names...),
		MatchAny: true,
		Override: false,
		Parent:   currentExpr,
	}

	featureFlags(fge)
}

func OverrideFeatureFlags(names ...string) {
	currentExpr := eval.Current()
	fge := &expr.FeatureFlagExpr{
		Flags:    names,
		MatchAny: true,
		Override: true,
		Parent:   currentExpr,
	}

	featureFlags(fge)
}
func OverrideFeatureFlagsAny(names ...string) {
	currentExpr := eval.Current()
	fge := &expr.FeatureFlagExpr{
		Flags:    names,
		MatchAny: true,
		Override: true,
		Parent:   currentExpr,
	}

	featureFlags(fge)
}

func featureFlags(fge *expr.FeatureFlagExpr) {
	currentExpr := eval.Current()

	switch current := currentExpr.(type) {
	case *goaexpr.ServiceExpr:
		expr.Root.ServiceFlags[current.Name] = append(expr.Root.ServiceFlags[current.Name], fge)
	case *goaexpr.MethodExpr:
		_, ok := expr.Root.ServiceMethodFlags[current.Service.Name]
		if !ok {
			expr.Root.ServiceMethodFlags[current.Service.Name] = map[string][]*expr.FeatureFlagExpr{}
		}
		expr.Root.ServiceMethodFlags[current.Service.Name][current.Name] = append(expr.Root.ServiceMethodFlags[current.Service.Name][current.Name], fge)
	default:
		eval.IncompatibleDSL()
	}
}
