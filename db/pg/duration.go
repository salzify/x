package pg

import (
	"database/sql/driver"
	"fmt"
	"strings"
	"time"
)

// This method is inspired from https://stackoverflow.com/a/73466373
// We can also use this package https://pkg.go.dev/github.com/sanyokbig/pqinterval#section-readme

// Duration wraps a time.Duration to provide implementations of
// sql.Scanner and driver.Valuer for reading/writing from/to a DB.
type Duration time.Duration

// Value converts Duration to a primitive value ready to written to a database.
func (d Duration) Value() (driver.Value, error) {
	return d.String(), nil
}

// Scan converts the received string in the format hh:mm:ss into a PgDuration.
// FIXME ALERT: It doesn't support format like (2 days, ...)
func (d *Duration) Scan(value interface{}) error {
	var s string
	switch v := value.(type) {
	case string:
		s = v
	case []byte:
		s = string(v)
	case nil:
		s = "00:00:00"
	default:
		return fmt.Errorf("cannot sql.Scan() PgDuration from: %#v", v)
	}
	// Convert format of hh:mm:ss (e.g 12:35:20) into format parsable by time.ParseDuration()
	s = strings.Replace(s, ":", "h", 1) //
	s = strings.Replace(s, ":", "m", 1)
	s += "s"
	dur, err := time.ParseDuration(s)
	if err != nil {
		return err
	}
	*d = Duration(dur)
	return nil
}

func NewPgDurationFromTimeDuration(in time.Duration) *Duration {
	d := Duration(int64(in))
	return &d
}

func (d Duration) ToTimeDuration() time.Duration {
	return time.Duration(d)
}

func (d Duration) Microseconds() int64 { return d.ToTimeDuration().Microseconds() }

func (d Duration) Milliseconds() int64 { return d.ToTimeDuration().Milliseconds() }

func (d Duration) Seconds() float64 {
	return d.ToTimeDuration().Seconds()
}

// Minutes returns the duration as a floating point number of minutes.
func (d Duration) Minutes() float64 {
	return d.ToTimeDuration().Minutes()
}

// Hours returns the duration as a floating point number of hours.
func (d Duration) Hours() float64 {
	return d.ToTimeDuration().Hours()
}

// String returns a string representing the duration in the form "72h3m0.5s".
// Leading zero units are omitted. As a special case, durations less than one
// second format use a smaller unit (milli-, micro-, or nanoseconds) to ensure
// that the leading digit is non-zero. The zero duration formats as 0s.
func (d Duration) String() string {
	return d.ToTimeDuration().String()
}
