package pg

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
)

type JsonData struct {
	json.RawMessage
}

func NewJsonData(data interface{}) (*JsonData, error) {
	d := &JsonData{}
	err := d.Set(data)
	if err != nil {
		return nil, err
	}
	return d, nil
}

func (j *JsonData) Set(in interface{}) error {
	if isNil(in) {
		j.RawMessage = nil
		return nil
	}
	raw, err := json.Marshal(in)
	if err != nil {
		return err
	}
	j.RawMessage = raw
	return nil
}

func (j *JsonData) Get(out interface{}) error {
	if len(j.RawMessage) == 0 {
		out = nil
		return nil
	}
	// use vanilla []byte which don't have UnmarshalJSON method
	var bytes []byte = j.RawMessage
	return json.Unmarshal(bytes, out)
}

// Value get value of Jsonb
func (j JsonData) Value() (driver.Value, error) {
	if len(j.RawMessage) == 0 {
		return nil, nil
	}
	jd, err := j.MarshalJSON()
	if err != nil {
		return nil, err
	}
	// trick pgx/v5 stdlib interface to use correctly format the value
	return json.RawMessage(jd), nil
}

// Scan scan value into Jsonb
func (j *JsonData) Scan(value interface{}) error {
	bytes, ok := value.([]byte)
	if !ok {
		return errors.New(fmt.Sprint("unable to unmarshal JSONB value:", value))
	}

	return json.Unmarshal(bytes, j)
}

func (j *JsonData) IsZero() bool {
	return j == nil || len(j.RawMessage) == 0
}

func isNil(i interface{}) bool {
	rv := reflect.ValueOf(i)
	if !rv.IsValid() {
		return true
	}
	for rv.Kind() == reflect.Interface {
		rv = rv.Elem()
	}

	switch rv.Kind() {
	case reflect.Map, reflect.Ptr, reflect.Slice, reflect.UnsafePointer:
		// nil-able kinds that can be json encoded/decoded.
		return rv.IsNil()
	default:
		return false
	}
}
