package errorgroup

import (
	"encoding/json"
	"fmt"
	"sort"
	"strings"
)

type ErrorGroup map[string]error

// Error returns the error string of ErrorGroup.
func (es ErrorGroup) Error() string {
	if len(es) == 0 {
		return ""
	}

	keys := []string{}
	for key := range es {
		keys = append(keys, key)
	}
	sort.Strings(keys)

	var s strings.Builder
	for i, key := range keys {
		if i > 0 {
			s.WriteString("; ")
		}
		if errs, ok := es[key].(ErrorGroup); ok {
			fmt.Fprintf(&s, "%v: (%v)", key, errs)
		} else {
			fmt.Fprintf(&s, "%v: %v", key, es[key].Error())
		}
	}
	s.WriteString(".")
	return s.String()
}

// MarshalJSON converts the ErrorGroup into a valid JSON.
func (es ErrorGroup) MarshalJSON() ([]byte, error) {
	errs := map[string]interface{}{}
	for key, err := range es {
		if ms, ok := err.(json.Marshaler); ok {
			errs[key] = ms
		} else {
			errs[key] = err.Error()
		}
	}
	return json.Marshal(errs)
}

func (es ErrorGroup) Err() error {
	if len(es) > 0 {
		return es
	}
	return nil
}
