package recoverable

import (
	"bytes"
	"fmt"
	"runtime"
	"strconv"
)

// GetStack returns a nicely formatted stack frame, skipping skip frames.
func GetStack(skip int) []byte {
	buffer := bytes.Buffer{}
	pcs := make([]uintptr, 64)

	var numFrames int
	for {
		// Skip the call to runtime.Counters and takeStacktrace so that the
		// program counters start at the caller of takeStacktrace.
		numFrames = runtime.Callers(skip+2, pcs)
		if numFrames < len(pcs) {
			break
		}
		// Don't put the too-short counter slice back into the pool; this lets
		// the pool adjust if we consistently take deep stacktraces.
		pcs = make([]uintptr, len(pcs)*2)
	}

	i := 0
	frames := runtime.CallersFrames(pcs[:numFrames])

	// Note: On the last iteration, frames.Next() returns false, with a valid
	// frame, but we ignore this frame. The last frame is a a runtime frame which
	// adds noise, since it's only either runtime.main or runtime.goexit.
	for frame, more := frames.Next(); more; frame, more = frames.Next() {
		if i != 0 {
			buffer.WriteByte('\n')
		}
		i++
		buffer.WriteString(frame.Function)
		buffer.WriteByte('\n')
		buffer.WriteByte('\t')
		buffer.WriteString(frame.File)
		buffer.WriteByte(':')
		buffer.WriteString(strconv.Itoa(frame.Line))
	}
	buffer.WriteByte('\n')

	return buffer.Bytes()
}

func DoWithRecoverStackSkip(f func(), skip int) (stack []byte, err error) {
	defer func() {
		if e := recover(); e != nil {
			if ee, ok := e.(error); ok {
				err = ee
			} else {
				err = fmt.Errorf("panic: %s", e)
			}
			// skip call to GetStack
			stack = GetStack(skip + 1)
		}
	}()

	f()

	return
}

func DoWithRecoverStack(f func()) ([]byte, error) {
	return DoWithRecoverStackSkip(f, 1)
}
