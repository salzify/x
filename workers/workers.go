package workers

import (
	"fmt"
	"strconv"
	"sync"

	"go.uber.org/atomic"

	"go.e.salzify.com/x/error-group"

	"github.com/gammazero/workerpool"
)

type workers struct {
	submitted  int
	stopOnFail bool

	w *workerpool.WorkerPool

	eg errorgroup.ErrorGroup
	m  *sync.Mutex

	stopperOnce *sync.Once
	stop        chan struct{}
	stopped     *atomic.Bool
}

func NewWorkers(
	maxWorkers int,
	stopOnFail bool,
) *workers {
	return &workers{
		submitted:  0,
		stopOnFail: stopOnFail,

		w: workerpool.New(maxWorkers),

		eg: errorgroup.ErrorGroup{},
		m:  &sync.Mutex{},

		stopperOnce: &sync.Once{},
		stop:        make(chan struct{}, 1),
		stopped:     atomic.NewBool(false),
	}
}

func (w *workers) doer(f func() error) func() {
	w.submitted += 1
	index := w.submitted
	return func() {
		var err error
		defer func() {
			if e := recover(); e != nil {
				err = fmt.Errorf("workers panic recovered: %v", e)
			}
			if err != nil {
				w.m.Lock()
				defer w.m.Unlock()
				w.eg[strconv.Itoa(index)] = err
				if w.stopOnFail {
					w.ForceStop()
				}
			}
		}()
		err = f()
	}
}

func (w *workers) stopper() {
	w.stopperOnce.Do(func() {
		go func() {
			defer func() {
				w.stopped.Store(true)
				close(w.stop)
			}()
			for {
				select {
				case <-w.stop:
					if !w.w.Stopped() {
						w.w.Stop()
					}
					return
				}
			}
		}()
	})
}

func (w *workers) Go(f func() error) {
	w.stopper()
	w.w.Submit(w.doer(f))
}

func (w *workers) Wait() error {
	w.w.StopWait()
	w.ForceStop()
	return w.Err()
}

func (w *workers) ForceStop() {
	if w.stopped.Load() {
		return
	}
	w.stopped.Store(true)
	w.stop <- struct{}{}
}

func (w *workers) WaitChan() chan error {
	errChan := make(chan error, 1)
	go func() {
		defer close(errChan)
		errChan <- w.Wait()
	}()
	return errChan
}

func (w *workers) Err() error {
	return w.eg.Err()
}
