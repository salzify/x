package priorityqueue

import (
	"container/heap"
	"fmt"
	"sync"
)

type Item struct {
	Priority int
	Data     interface{}
}

type items []*Item

func (pq items) Len() int { return len(pq) }

func (pq items) Less(i, j int) bool {
	// We want Pop to give us the lowest, not highest, priority so we use less than here.
	return pq[i].Priority < pq[j].Priority
}

func (pq items) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
}

func (pq *items) Push(x interface{}) {
	item := x.(*Item)
	*pq = append(*pq, item)
}

func (pq *items) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	*pq = old[0 : n-1]
	return item
}

func (pq *items) Peak() *Item {
	return (*pq)[0]
}

// PriorityQueue implements heap.Interface and holds items.
type PriorityQueue struct {
	items
	mu sync.Mutex
}

func NewPriorityQueue() *PriorityQueue {
	pq := &PriorityQueue{}
	heap.Init(pq)
	return pq
}

func (pq *PriorityQueue) Len() int {
	pq.mu.Lock()
	defer pq.mu.Unlock()
	return pq.items.Len()
}

func (pq *PriorityQueue) Less(i, j int) bool {
	pq.mu.Lock()
	defer pq.mu.Unlock()
	return pq.items.Less(i, j)
}

func (pq *PriorityQueue) Swap(i, j int) {
	pq.mu.Lock()
	defer pq.mu.Unlock()
	pq.items.Swap(i, j)
}

func (pq *PriorityQueue) Push(x interface{}) {
	pq.mu.Lock()
	defer pq.mu.Unlock()
	pq.items.Push(x)
}

func (pq *PriorityQueue) Pop() interface{} {
	pq.mu.Lock()
	defer pq.mu.Unlock()
	return pq.items.Pop()
}

func (pq *PriorityQueue) Peak() *Item {
	pq.mu.Lock()
	defer pq.mu.Unlock()
	return pq.items.Peak()
}

func PrioritizeInOrder(in <-chan *Item, out chan<- interface{}, startingPriority int) error {
	q := NewPriorityQueue()

	nextPriority := startingPriority
	for item := range in {
		q.mu.Lock()

		// push new item to the priority queue
		heap.Push(&q.items, item)

		for peak := q.items.Peak(); peak.Priority == nextPriority; peak = q.items.Peak() {
			heap.Pop(&q.items)
			out <- peak.Data
			nextPriority++

			if q.items.Len() == 0 {
				break
			}
		}
		if q.items.Len() > 0 {
			peak := q.items.Peak()
			if peak.Priority < nextPriority {
				q.mu.Unlock()
				return fmt.Errorf("item.Priority:%v is less than nextPriority:%v", item.Priority, nextPriority)
			}
		}
		q.mu.Unlock()
	}

	return nil
}
