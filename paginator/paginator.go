package paginator

import (
	"math"

	"go.e.salzify.com/x/priority-queue"
	"go.e.salzify.com/x/workers"
)

const (
	DefaultPageSize   = 1000
	DefaultMaxWorkers = 3
)

type (
	counterFunc  func() (*int, error)
	accessorFunc func(limit, offset int) (interface{}, error)
)

func Paginate(counter counterFunc, accessor accessorFunc, pageSize int, maxWorkers int) (results chan interface{}, errChan chan error) {
	results = make(chan interface{})
	errChan = make(chan error, 1)

	if pageSize < 1 {
		pageSize = DefaultPageSize
	}
	if maxWorkers < 1 {
		maxWorkers = DefaultMaxWorkers
	}

	totalCount, err := counter()
	if err != nil {
		errChan <- err
		close(errChan)
		close(results)
		return
	}

	if *totalCount < 1 {
		close(errChan)
		close(results)
		return
	}

	totalPages := int(math.Ceil(float64(*totalCount) / float64(pageSize)))

	w := workers.NewWorkers(maxWorkers, true)

	priorityIn := make(chan *priorityqueue.Item, totalPages)

	getPage := func(page, offset int) {
		w.Go(func() error {
			items, err := accessor(pageSize, offset)
			if err != nil {
				return err
			}

			priorityIn <- &priorityqueue.Item{
				Priority: page,
				Data:     items,
			}
			return nil
		})
	}

	priorityErr := make(chan error, 1)
	go func() {
		// this goroutine is the sender for those two channels, so it closes them
		defer close(priorityErr)
		defer close(results) // Prioritize sends messages to results
		err := priorityqueue.PrioritizeInOrder(priorityIn, results, 1)
		if err != nil {
			priorityErr <- err
		}
	}()

	page := 1
	offset := 0

	for page <= totalPages {
		getPage(page, offset)
		page++
		offset += pageSize
	}

	go func() {
		// this goroutine is the sender for those two channels, so it closes them
		defer close(priorityIn) // Worker doer functions are the senders of priorityIn
		defer close(errChan)
		workersErr := w.WaitChan()

		var err error
		for isStopped := false; !isStopped; {
			select {
			case err = <-priorityErr:
				w.ForceStop()
				isStopped = true
			case err = <-workersErr:
				isStopped = true
			}
		}
		if err != nil {
			errChan <- err
		}
	}()

	return
}
